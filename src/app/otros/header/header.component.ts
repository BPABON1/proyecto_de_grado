import { Component, OnInit } from '@angular/core';
import { AlertasService } from 'src/app/service/alertas.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import {AppComponent} from 'src/app/app.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public permisos: any =[];
  public usuario:any;
  constructor(
    private alerta: AlertasService,
    private route: Router,
    private app: AppComponent,
  ) { }

  ngOnInit(): void {
    this.ListPermisos();
    this.ListarInfoUusario();
    this.app.ValidarSesionCerrada();
  }
  /*
  fecha: 14-03-2022
  proposito: Listar todos los permisos del usuario para mostra el menu
  */
 ListPermisos(){
   this.permisos = localStorage.getItem('permisos');
  // console.log(this.permisos);
 }
 /*
 fecha: 14-03-2022
 proposito:  Listar informacion detallada del usuario 
 */
ListarInfoUusario(){
  var datos:any = localStorage.getItem('usuario');
  this.usuario = JSON.parse(datos);
  console.log(this.usuario);
}
/*
* fecha: 05-04-2022
* propoito: Mostrar alerta si se desea cerrar la sesion en caso de que si eliminar la variable de sesion
*/
CerrarSesion(){
  try {
    Swal.fire({
      title: `Esta seguro de cerrar la sesión`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SI',
      cancelButtonText: 'NO',
    }).then((result) => {
      if (result.isConfirmed) {
        localStorage.removeItem('permisos');
        localStorage.removeItem('usuario');
        localStorage.removeItem('token');
        this.route.navigateByUrl('/login');
        this.alerta.exito('Sesión cerrada de forma correcta ');
      }
    });
  } catch (error:any) {
    this.alerta.error(error.message);
  }
}
}
