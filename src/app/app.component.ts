import { Component } from '@angular/core';
import { Spinkit } from 'ng-http-loader';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'proyecto-grado';
  public spinkit = Spinkit;
  constructor(
    private route: Router,
  ){
  }
  ngOnInit(): void {
    if(!localStorage.getItem('usuario')){
      this.route.navigateByUrl('/login');
    }
  }
  ValidarSesionCerrada(){
    console.log('Validar sesion cerrada');
    if(!localStorage.getItem('usuario')){
      this.route.navigateByUrl('/login');
    }
  }
  ValidarSesionIniciada(){
    console.log('Validar sesion iniciada');
    if(localStorage.getItem('usuario')){
      this.route.navigateByUrl('/home');
    }
  }
}

