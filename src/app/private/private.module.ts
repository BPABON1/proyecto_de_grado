import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OtrosModule } from '../otros/otros.module';
import { PrivateRoutingModule } from './private.routing';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsuariosComponent } from './administracion/usuarios/usuarios.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatSliderModule } from '@angular/material/slider';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSelectModule } from '@angular/material/select';
import { ProjectsUsuariosComponent } from './administracion/projects-usuarios/projects-usuarios.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { EntregarComponent } from './proyectos/entregar/entregar.component';
import { ListaEvaluarComponent } from './proyectos/lista-evaluar/lista-evaluar.component';
import { ShowEvaluarComponent } from './proyectos/show-evaluar/show-evaluar.component';
import { PublicarComponent } from './proyectos/publicar/publicar.component';
import { ListarBuscadorComponent } from './proyectos/listar-buscador/listar-buscador.component';
import { DetalleBuscadorComponent } from './proyectos/detalle-buscador/detalle-buscador.component';
import { AsignacionProjectsComponent } from './administracion/asignacion-projects/asignacion-projects.component';
import { TiposEntregasComponent } from './proyectos/tipos-entregas/tipos-entregas.component';
import { EntregarAnteProyectoComponent } from './proyectos/entregar-ante-proyecto/entregar-ante-proyecto.component';
import { TipoEvaluacionComponent } from './proyectos/tipo-evaluacion/tipo-evaluacion.component';
import { EvaluarAnteProyectoComponent } from './proyectos/evaluar-ante-proyecto/evaluar-ante-proyecto.component';
import { ListaEvaluarAnteProyectoComponent } from './proyectos/lista-evaluar-ante-proyecto/lista-evaluar-ante-proyecto.component';

@NgModule({
  declarations: [
    DashboardComponent,
    UsuariosComponent,
    ProjectsUsuariosComponent,
    EntregarComponent,
    ListaEvaluarComponent,
    ShowEvaluarComponent,
    PublicarComponent,
    ListarBuscadorComponent,
    DetalleBuscadorComponent,
    AsignacionProjectsComponent,
    TiposEntregasComponent,
    EntregarAnteProyectoComponent,
    TipoEvaluacionComponent,
    EvaluarAnteProyectoComponent,
    ListaEvaluarAnteProyectoComponent
  ],
  imports: [
    CommonModule,
    PrivateRoutingModule,
    OtrosModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    FormsModule,
    ReactiveFormsModule,
    MatSliderModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatSelectModule,
    NgSelectModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class PrivateModule { }
