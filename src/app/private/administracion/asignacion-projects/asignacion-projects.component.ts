import { Component, OnInit } from '@angular/core';
import { AlertasService } from 'src/app/service/alertas.service';
import { AdministradorService } from 'src/app/service/administrador.service';
import { FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-asignacion-projects',
  templateUrl: './asignacion-projects.component.html',
  styleUrls: ['./asignacion-projects.component.css'],
})
export class AsignacionProjectsComponent implements OnInit {
  public ListaProjects: any[] = [];
  public pageinaActual: number = 1;
  public filtro = '';
  public NewAsignacionAnteProyecto = this.fb.group({
    titulo: this.fb.array([]),
    titulo_temporal: null,
    estudiantes: this.fb.array([]),
    estudiante_temporal: null,
    director: this.fb.array([]),
    director_temporal: null,
    evaluador: this.fb.array([]),
    evaluador_temporal: null,
  });
  public UpdateAsignacionAnteProyecto = this.fb.group({
    titulo: [null, Validators.required],
    name_titulo: '',
    estado: ['', Validators.required],
    estudiantes: this.fb.array([]),
    estudiante_temporal: null,
    director: this.fb.array([]),
    director_temporal: null,
    evaluador: this.fb.array([]),
    evaluador_temporal: null,
  });
  public UpdateAsignacionProyectoFinal = this.fb.group({
    titulo: [null, Validators.required],
    name_titulo: '',
    estado: ['', Validators.required],
    estudiantes: this.fb.array([]),
    estudiante_temporal: null,
    director: this.fb.array([]),
    director_temporal: null,
    evaluador: this.fb.array([]),
    evaluador_temporal: null,
  });
  public ProjectPendientes: any[] = [];
  public ListaEstudiantes: any[] = [];
  public ListaDirectoresProyectos: any[] = [];
  public ListaEvaluadoresProyectos: any[] = [];
  public EtapasProyecto: any[] = [];
  constructor(
    private alerta: AlertasService,
    private fb: FormBuilder,
    private administrador: AdministradorService
  ) {}

  ngOnInit(): void {
    this.CargarDatos();
  }

  // Listar el total de ante proyectos y proyectos de grados finales pendientes por culminar
  async CargarDatos() {
    this.ListaProjects = [];
    this.administrador.SearchFasesProyectos().subscribe(
      (resp) => {
        var data: any = resp;
        this.ListaEstudiantes = data.resp.TotalEstudiantesActivos;
        this.ProjectPendientes = data.resp.TotalProyectosLibres;
        this.ListaDirectoresProyectos = data.resp.TotalDirectoresActivos;
        this.ListaEvaluadoresProyectos = data.resp.TotalEvaluadoresActivos;
        this.EtapasProyecto = data.resp.EtapasProyecto;
        console.log(data);
      },
      (err: any) => {
        if (!err.error.msg) {
          const errores = err.error.error.map((error: any) => {
            return error.msg;
          });
          this.alerta.error(errores.toString());
        } else {
          this.alerta.error(err.error.msg);
        }
      }
    );
  }
  // Guardar la asignacion de ante proyecto con estudiantes director y evaluadores
  RegistrarAsignacion() {
    try {
      console.log(this.NewAsignacionAnteProyecto.value.evaluador);
      console.log(this.NewAsignacionAnteProyecto.value.evaluador.length );
      if (this.NewAsignacionAnteProyecto.value.titulo.length === 1) {
        if (
          this.NewAsignacionAnteProyecto.value.estudiantes.length > 0 &&
          this.NewAsignacionAnteProyecto.value.estudiantes.length < 4
        ) {
          if (this.NewAsignacionAnteProyecto.value.director.length === 1) {
            if (
              this.NewAsignacionAnteProyecto.value.evaluador.length > 0 &&
              this.NewAsignacionAnteProyecto.value.evaluador.length <= 3
            ) {
              this.administrador
                .SaveAnteProyectos(this.NewAsignacionAnteProyecto.value)
                .subscribe(
                  (resp) => {
                    var data: any = resp;
                    this.alerta.exito(data.resp.msg);
                    this.CargarDatos();
                    console.log(data);
                  },
                  (err: any) => {
                    console.log(err);
                    if (!err.error.msg) {
                      const errores = err.error.error.map((error: any) => {
                        return error.msg;
                      });
                      this.alerta.error(errores.toString());
                    } else {
                      this.alerta.error(err.error.msg);
                    }
                  }
                );
            } else {
              this.alerta.error('Se debe seleccionar mas de 1 evaluador');
            }
          } else {
            this.alerta.error(
              'Se debe seleccionar 1 director de ante proyecto'
            );
          }
        } else {
          this.alerta.error(
            'Se debe seleccionar al menos 1 estudiante a cargo del ante proyecto'
          );
        }
      } else {
        this.alerta.error('Se debe seleccionar almenos 1 titulo de proyecto');
      }
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Agregar ante proyecto seleccionado a la lista que se desea guardar
  AgregarTituloAnteProyecto() {
    try {
      if (this.NewAsignacionAnteProyecto.value.titulo_temporal !== null) {
        if (this.NewAsignacionAnteProyecto.value.titulo.length === 0) {
          let datos = {
            id: null,
            bandera: 'NUEVO',
            nombre: '',
          };
          var ingreso = 'NO';
          var existe = 'NO';
          for (let info of this.ProjectPendientes) {
            if (
              info.id === this.NewAsignacionAnteProyecto.value.titulo_temporal
            ) {
              ingreso = 'SI';
              datos = {
                id: info.id,
                bandera: 'NUEVO',
                nombre: info.titulo,
              };
            }
          }
          for (let data of this.NewAsignacionAnteProyecto.value.titulo) {
            if (
              data.id === this.NewAsignacionAnteProyecto.value.titulo_temporal
            ) {
              existe = 'SI';
            }
          }
          if (existe !== 'SI') {
            if (ingreso === 'SI') {
              this.NewAsignacionAnteProyecto.value.titulo.push(datos);
              console.log(this.NewAsignacionAnteProyecto.value);
            } else {
              this.alerta.error('No fue posible agregar el proyecto');
            }
          } else {
            this.alerta.info(
              'El proyecto ya se encuentra en la lista seleccionada.'
            );
          }
        } else {
          this.alerta.error(
            'No se debe seleccionar mas de 1 proyecto, para cambiarlo por favor elimine el que tiene asociado'
          );
        }
      } else {
        this.alerta.error('Por favor seleccione un proyecto valido');
      }
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Agregar estudiantes a cargo del ante proyecto
  AgregarEstudianteAnteProyecto() {
    try {
      if (this.NewAsignacionAnteProyecto.value.estudiante_temporal !== null) {
        if (this.NewAsignacionAnteProyecto.value.estudiantes.length < 4) {
          let datos = {
            id: null,
            bandera: 'NUEVO',
            nombre: '',
          };
          var ingreso = 'NO';
          var existe = 'NO';
          for (let info of this.ListaEstudiantes) {
            if (
              info.id ===
              this.NewAsignacionAnteProyecto.value.estudiante_temporal
            ) {
              ingreso = 'SI';
              datos = {
                id: info.id,
                bandera: 'NUEVO',
                nombre: info.nombre + ' ' + info.apellidos,
              };
            }
          }
          for (let data of this.NewAsignacionAnteProyecto.value.estudiantes) {
            if (
              data.id ===
              this.NewAsignacionAnteProyecto.value.estudiante_temporal
            ) {
              existe = 'SI';
            }
          }
          if (existe !== 'SI') {
            if (ingreso === 'SI') {
              this.NewAsignacionAnteProyecto.value.estudiantes.push(datos);
              console.log(this.NewAsignacionAnteProyecto.value);
            } else {
              this.alerta.error('No fue posible agregar el estudiante');
            }
          } else {
            this.alerta.info(
              'El estudiante ya se encuentra en la lista seleccionada.'
            );
          }
        } else {
          this.alerta.error(
            'El numero maximo de estudiantes a cargo de un proyecto es de 4'
          );
        }
      } else {
        this.alerta.error('Por favor seleccione un estudiante valido');
      }
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Agregar director de ante proyecto
  AgregarDirectorAnteProyecto() {
    try {
      if (this.NewAsignacionAnteProyecto.value.director_temporal !== null) {
        if (this.NewAsignacionAnteProyecto.value.director.length === 0) {
          let datos = {
            id: null,
            bandera: 'NUEVO',
            nombre: '',
          };
          var ingreso = 'NO';
          var existe = 'NO';
          var isEvaluador = 'NO';
          for (let info of this.ListaDirectoresProyectos) {
            if (
              info.id === this.NewAsignacionAnteProyecto.value.director_temporal
            ) {
              ingreso = 'SI';
              datos = {
                id: info.id,
                bandera: 'NUEVO',
                nombre: info.nombre + ' ' + info.apellidos,
              };
            }
          }
          if (this.NewAsignacionAnteProyecto.value.evaluador.length > 0) {
            for (let data of this.NewAsignacionAnteProyecto.value.evaluador) {
              if (
                data.id ===
                this.NewAsignacionAnteProyecto.value.director_temporal
              ) {
                isEvaluador = 'SI';
              }
            }
          }
          for (let data of this.NewAsignacionAnteProyecto.value.director) {
            if (
              data.id === this.NewAsignacionAnteProyecto.value.director_temporal
            ) {
              existe = 'SI';
            }
          }
          if (existe !== 'SI') {
            if (ingreso === 'SI') {
              if (isEvaluador === 'NO') {
                this.NewAsignacionAnteProyecto.value.director.push(datos);
              } else {
                this.alerta.error(
                  'El director no puede ser evaluador de proyecto'
                );
              }
            } else {
              this.alerta.error('No fue posible agregar el director');
            }
          } else {
            this.alerta.info(
              'El director ya se encuentra en la lista seleccionada.'
            );
          }
        } else {
          this.alerta.error(
            'No se debe seleccionar mas de 1 director de proyecto, para cambiarlo por favor elimine el que tiene asociado'
          );
        }
      } else {
        this.alerta.error('Por favor seleccione un director valido');
      }
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Argregar evaluadores de ante proyecto
  AgregarEvaluadoresAnteProyecto() {
    try {
      if (this.NewAsignacionAnteProyecto.value.evaluador_temporal !== null) {
        if (this.NewAsignacionAnteProyecto.value.evaluador.length < 3) {
          let datos = {
            id: null,
            bandera: 'NUEVO',
            nombre: '',
          };
          var ingreso = 'NO';
          var existe = 'NO';
          var isDirector = 'NO';
          for (let info of this.ListaEvaluadoresProyectos) {
            if (
              info.id ===
              this.NewAsignacionAnteProyecto.value.evaluador_temporal
            ) {
              ingreso = 'SI';
              datos = {
                id: info.id,
                bandera: 'NUEVO',
                nombre: info.nombre + ' ' + info.apellidos,
              };
            }
          }
          if (this.NewAsignacionAnteProyecto.value.director.length > 0) {
            for (let data of this.NewAsignacionAnteProyecto.value.director) {
              if (
                data.id ===
                this.NewAsignacionAnteProyecto.value.evaluador_temporal
              ) {
                isDirector = 'SI';
              }
            }
          }
          for (let data of this.NewAsignacionAnteProyecto.value.evaluador) {
            if (
              data.id ===
              this.NewAsignacionAnteProyecto.value.evaluador_temporal
            ) {
              existe = 'SI';
            }
          }
          if (existe !== 'SI') {
            if (ingreso === 'SI') {
              if (isDirector === 'NO') {
                this.NewAsignacionAnteProyecto.value.evaluador.push(datos);
              } else {
                this.alerta.error(
                  'El evaluador no puede ser director de proyecto'
                );
              }
            } else {
              this.alerta.error('No fue posible agregar el evaluador');
            }
          } else {
            this.alerta.info(
              'El evaluador ya se encuentra en la lista seleccionada.'
            );
          }
        } else {
          this.alerta.error(
            'No se debe seleccionar mas de 3 evaluadores de proyecto'
          );
        }
      } else {
        this.alerta.error('Por favor seleccione un evaluador valido');
      }
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Eliminar el titulo temporalmente
  EliminarTemporalTitulo(item: any) {
    var i = this.NewAsignacionAnteProyecto.value.titulo.indexOf(item);
    this.NewAsignacionAnteProyecto.value.titulo.splice(i, 1);
  }
  // Eliminar el Estudiante temporalmente
  EliminarTemporalEstudiante(item: any) {
    var i = this.NewAsignacionAnteProyecto.value.estudiantes.indexOf(item);
    this.NewAsignacionAnteProyecto.value.estudiantes.splice(i, 1);
  }
  // Eliminar el director temoralmente
  EliminarTemporalDirector(item: any) {
    var i = this.NewAsignacionAnteProyecto.value.director.indexOf(item);
    this.NewAsignacionAnteProyecto.value.director.splice(i, 1);
  }
  // Eliminar el evaluador temoralmente
  EliminarTemporalEvaluador(item: any) {
    var i = this.NewAsignacionAnteProyecto.value.evaluador.indexOf(item);
    this.NewAsignacionAnteProyecto.value.evaluador.splice(i, 1);
  }
  // Actualizar ante proyecto
  UpdateAnteProyecto() {
    try {
      if (this.UpdateAsignacionAnteProyecto.invalid) {
        this.alerta.error(
          'Formulario invalido por favor verifique que todos los datos esten ingresados'
        );
      } else {
        if (
          this.UpdateAsignacionAnteProyecto.value.estudiantes.length > 0 &&
          this.UpdateAsignacionAnteProyecto.value.estudiantes.length < 4
        ) {
          if (this.UpdateAsignacionAnteProyecto.value.director.length === 1) {
            if (
              this.UpdateAsignacionAnteProyecto.value.evaluador.length > 0 &&
              this.UpdateAsignacionAnteProyecto.value.evaluador.length < 4
            ) {
              this.administrador
                .UpdateAnteProyectos(this.UpdateAsignacionAnteProyecto.value)
                .subscribe(
                  (resp) => {
                    var data: any = resp;
                    this.alerta.exito(data.resp.msg);
                    this.CargarDatos();
                    console.log(data);
                  },
                  (err: any) => {
                    console.log(err);
                    if (!err.error.msg) {
                      const errores = err.error.error.map((error: any) => {
                        return error.msg;
                      });
                      this.alerta.error(errores.toString());
                    } else {
                      this.alerta.error(err.error.msg);
                    }
                  }
                );
            } else {
              this.alerta.error('Se debe seleccionar mas de 1 evaluador');
            }
          } else {
            this.alerta.error(
              'Se debe seleccionar 1 director de ante proyecto'
            );
          }
        } else {
          this.alerta.error(
            'Se debe seleccionar al menos 1 estudiante a cargo del ante proyecto'
          );
        }
      }
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Pasar los datos para actualizar el ante proyecto
  DatosAnteProyecto(data) {
    try {
      console.log(data);
      this.UpdateAsignacionAnteProyecto.patchValue({
        titulo: data.id,
        estado: data.estado,
        name_titulo: data.titulo,
        estudiantes: [],
        director: [],
        evaluador: [],
      });
      if (data.StudentProjects.length > 0) {
        for (let item of data.StudentProjects) {
          var datos = {
            id: item.Usuario.id,
            bandera: 'UPDATE',
            nombre: item.Usuario.nombre + ' ' + item.Usuario.apellidos,
            id_relacional: item.id,
          };
          this.UpdateAsignacionAnteProyecto.value.estudiantes.push(datos);
        }
      }
      if (data.DocentesAnteProyecto.length > 0) {
        if (data.DocentesAnteProyecto[0].DirectorAnteProyecto !== null) {
          var datos = {
            id: data.DocentesAnteProyecto[0].DirectorAnteProyecto.id,
            bandera: 'UPDATE',
            nombre:
              data.DocentesAnteProyecto[0].DirectorAnteProyecto.nombre +
              ' ' +
              data.DocentesAnteProyecto[0].DirectorAnteProyecto.apellidos,
            id_relacional: data.id,
          };
          this.UpdateAsignacionAnteProyecto.value.director.push(datos);
        }
      }
      if (data.DocentesAnteProyecto.length > 0) {
        for (let item of data.DocentesAnteProyecto) {
          var datos = {
            id: item.EvaluadorAnteProyecto.id,
            bandera: 'UPDATE',
            nombre:
              item.EvaluadorAnteProyecto.nombre +
              ' ' +
              item.EvaluadorAnteProyecto.apellidos,
            id_relacional: item.id,
          };
          this.UpdateAsignacionAnteProyecto.value.evaluador.push(datos);
        }
      }
      console.log(this.UpdateAsignacionAnteProyecto.value);
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Agregar estudiantes relacionados al ante proyecto
  async AgregarEstudianteAnteProyectoEdit() {
    try {
      if (
        this.UpdateAsignacionAnteProyecto.value.estudiante_temporal !== null
      ) {
        if (this.UpdateAsignacionAnteProyecto.value.estudiantes.length < 4) {
          let datos = {
            id: null,
            bandera: 'NUEVO',
            nombre: '',
          };
          var ingreso = 'NO';
          var existe = 'NO';
          for (let info of this.ListaEstudiantes) {
            if (
              info.id ===
              this.UpdateAsignacionAnteProyecto.value.estudiante_temporal
            ) {
              ingreso = 'SI';
              datos = {
                id: info.id,
                bandera: 'NUEVO',
                nombre: info.nombre + ' ' + info.apellidos,
              };
            }
          }
          for (let data of this.UpdateAsignacionAnteProyecto.value
            .estudiantes) {
            if (
              data.id ===
              this.UpdateAsignacionAnteProyecto.value.estudiante_temporal
            ) {
              existe = 'SI';
            }
          }
          if (existe !== 'SI') {
            if (ingreso === 'SI') {
              console.log(this.UpdateAsignacionAnteProyecto.value);
              this.UpdateAsignacionAnteProyecto.value.estudiantes.push(datos);
            } else {
              this.alerta.error('No fue posible agregar el estudiante');
            }
          } else {
            this.alerta.info(
              'El estudiante ya se encuentra en la lista seleccionada.'
            );
          }
        } else {
          this.alerta.error(
            'El numero maximo de estudiantes a cargo de un proyecto es de 4'
          );
        }
      } else {
        this.alerta.error('Por favor seleccione un estudiante valido');
      }
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Elminar el estudiante del ante proyecto
  EliminarTemporalEstudiante2(data) {
    try {
      this.administrador.DeleteEstudianteAnteProyectos(data).subscribe(
        (resp) => {
          var datos: any = resp;
          this.alerta.exito(datos.resp.msg);
          console.log(datos);
          var i =
            this.UpdateAsignacionAnteProyecto.value.estudiantes.indexOf(data);
          this.UpdateAsignacionAnteProyecto.value.estudiantes.splice(i, 1);
          this.CargarDatos();
        },
        (err: any) => {
          if (!err.error.msg) {
            const errores = err.error.error.map((error: any) => {
              return error.msg;
            });
            this.alerta.error(errores.toString());
          } else {
            this.alerta.error(err.error.msg);
          }
        }
      );
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Agregar un director de ante proyecto a uno ya almacenado
  AgregarDirectorAnteProyectoEdit() {
    try {
      if (this.UpdateAsignacionAnteProyecto.value.director_temporal !== null) {
        if (this.UpdateAsignacionAnteProyecto.value.director.length === 0) {
          let datos = {
            id: null,
            bandera: 'NUEVO',
            nombre: '',
          };
          var ingreso = 'NO';
          var existe = 'NO';
          var isEvaluador = 'NO';
          for (let info of this.ListaDirectoresProyectos) {
            if (
              info.id ===
              this.UpdateAsignacionAnteProyecto.value.director_temporal
            ) {
              ingreso = 'SI';
              datos = {
                id: info.id,
                bandera: 'NUEVO',
                nombre: info.nombre + ' ' + info.apellidos,
              };
            }
          }
          if (this.UpdateAsignacionAnteProyecto.value.evaluador.length > 0) {
            for (let data of this.UpdateAsignacionAnteProyecto.value
              .evaluador) {
              if (
                data.id ===
                this.UpdateAsignacionAnteProyecto.value.director_temporal
              ) {
                isEvaluador = 'SI';
              }
            }
          }
          for (let data of this.UpdateAsignacionAnteProyecto.value.director) {
            if (
              data.id ===
              this.UpdateAsignacionAnteProyecto.value.director_temporal
            ) {
              existe = 'SI';
            }
          }
          if (existe !== 'SI') {
            if (ingreso === 'SI') {
              if (isEvaluador === 'NO') {
                this.UpdateAsignacionAnteProyecto.value.director.push(datos);
              } else {
                this.alerta.error(
                  'El director no puede ser evaluador de proyecto'
                );
              }
            } else {
              this.alerta.error('No fue posible agregar el director');
            }
          } else {
            this.alerta.info(
              'El director ya se encuentra en la lista seleccionada.'
            );
          }
        } else {
          this.alerta.error(
            'No se debe seleccionar mas de 1 director de proyecto, para cambiarlo por favor elimine el que tiene asociado'
          );
        }
      } else {
        this.alerta.error('Por favor seleccione un director valido');
      }
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  EliminarTemporalDirectorEdit(data) {
    try {
      this.administrador.DeleteDirectorAnteProyectos(data).subscribe(
        (resp) => {
          var datos: any = resp;
          this.alerta.exito(datos.resp.msg);
          console.log(datos);
          var i =
            this.UpdateAsignacionAnteProyecto.value.director.indexOf(data);
          this.UpdateAsignacionAnteProyecto.value.director.splice(i, 1);
          this.CargarDatos();
        },
        (err: any) => {
          if (!err.error.msg) {
            const errores = err.error.error.map((error: any) => {
              return error.msg;
            });
            this.alerta.error(errores.toString());
          } else {
            this.alerta.error(err.error.msg);
          }
        }
      );
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Eliminar el director de ante proyecto temporal
  EliminarTemporalDirectorEdit2(data) {
    try {
      var i = this.UpdateAsignacionAnteProyecto.value.director.indexOf(data);
      this.UpdateAsignacionAnteProyecto.value.director.splice(i, 1);
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Agregar un nuevo evaluador al ante proyecto ya almacenado
  AgregarEvaluadoresAnteProyectoEdit() {
    try {
      if (this.UpdateAsignacionAnteProyecto.value.evaluador_temporal !== null) {
        if (this.UpdateAsignacionAnteProyecto.value.evaluador.length < 4) {
          let datos = {
            id: null,
            bandera: 'NUEVO',
            nombre: '',
          };
          var ingreso = 'NO';
          var existe = 'NO';
          var isDirector = 'NO';
          for (let info of this.ListaEvaluadoresProyectos) {
            if (
              info.id ===
              this.UpdateAsignacionAnteProyecto.value.evaluador_temporal
            ) {
              ingreso = 'SI';
              datos = {
                id: info.id,
                bandera: 'NUEVO',
                nombre: info.nombre + ' ' + info.apellidos,
              };
            }
          }
          if (this.UpdateAsignacionAnteProyecto.value.director.length > 0) {
            for (let data of this.UpdateAsignacionAnteProyecto.value.director) {
              if (
                data.id ===
                this.UpdateAsignacionAnteProyecto.value.evaluador_temporal
              ) {
                isDirector = 'SI';
              }
            }
          }
          for (let data of this.UpdateAsignacionAnteProyecto.value.evaluador) {
            if (
              data.id ===
              this.UpdateAsignacionAnteProyecto.value.evaluador_temporal
            ) {
              existe = 'SI';
            }
          }
          if (existe !== 'SI') {
            if (ingreso === 'SI') {
              if (isDirector === 'NO') {
                this.UpdateAsignacionAnteProyecto.value.evaluador.push(datos);
              } else {
                this.alerta.error(
                  'El evaluador no puede ser director de proyecto'
                );
              }
            } else {
              this.alerta.error('No fue posible agregar el evaluador');
            }
          } else {
            this.alerta.info(
              'El evaluador ya se encuentra en la lista seleccionada.'
            );
          }
        } else {
          this.alerta.error(
            'No se debe seleccionar mas de 3 evaluadores de proyecto'
          );
        }
      } else {
        this.alerta.error('Por favor seleccione un evaluador valido');
      }
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Eliminar un evaluador ya almacenado en la base de datos
  EliminarTemporalEvaluadorEdit(data) {
    try {
      if (this.UpdateAsignacionAnteProyecto.value.evaluador.length > 1) {
        this.administrador.DeleteEvaluadorAnteProyectos(data).subscribe(
          (resp) => {
            var datos: any = resp;
            this.alerta.exito(datos.resp.msg);
            console.log(datos);
            var i =
              this.UpdateAsignacionAnteProyecto.value.evaluador.indexOf(data);
            this.UpdateAsignacionAnteProyecto.value.evaluador.splice(i, 1);
            this.CargarDatos();
          },
          (err: any) => {
            if (!err.error.msg) {
              const errores = err.error.error.map((error: any) => {
                return error.msg;
              });
              this.alerta.error(errores.toString());
            } else {
              this.alerta.error(err.error.msg);
            }
          }
        );
      } else {
        this.alerta.error('Se debe mantener almenos 1 evaluador');
      }
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Eliminar un evaluador en temporal
  EliminarTemporalEvaluadorEdit2(data) {
    try {
      var i = this.UpdateAsignacionAnteProyecto.value.evaluador.indexOf(data);
      this.UpdateAsignacionAnteProyecto.value.evaluador.splice(i, 1);
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Eliminar el Estudiante temporalmente
  EliminarTemporalEstudiante1(item: any) {
    var i = this.UpdateAsignacionAnteProyecto.value.estudiantes.indexOf(item);
    this.UpdateAsignacionAnteProyecto.value.estudiantes.splice(i, 1);
  }
  // Pasar los datos para actualizar el proyecto final
  DatosProyectoFinal(data) {
    try {
      console.log(data);
      this.UpdateAsignacionProyectoFinal.patchValue({
        titulo: data.id,
        estado: data.estado,
        name_titulo: data.titulo,
        estudiantes: [],
        director: [],
        evaluador: [],
      });
      if (data.StudentProjects.length > 0) {
        for (let item of data.StudentProjects) {
          var datos = {
            id: item.Usuario.id,
            bandera: 'UPDATE',
            nombre: item.Usuario.nombre + ' ' + item.Usuario.apellidos,
            id_relacional: item.id,
          };
          this.UpdateAsignacionProyectoFinal.value.estudiantes.push(datos);
        }
      }
      if (data.DocentesAnteProyecto.length > 0) {
        let ingresoDirector = 'NO';
        for (let i of data.DocentesAnteProyecto) {
          if (ingresoDirector === 'NO' && i.tipo === 'PROYECTO FINAL') {
            if(i.DirectorAnteProyecto !== null){
              var datos = {
                id: i.DirectorAnteProyecto.id,
                bandera: 'UPDATE',
                nombre:
                  i.DirectorAnteProyecto.nombre +
                  ' ' +
                  i.DirectorAnteProyecto.apellidos,
                id_relacional: data.id,
              };
              this.UpdateAsignacionProyectoFinal.value.director.push(datos);
              ingresoDirector = 'SI';
            }
          }
        }
      }
      if (data.DocentesAnteProyecto.length > 0) {
        for (let item of data.DocentesAnteProyecto) {
          if (item.tipo === 'PROYECTO FINAL') {
            var datos = {
              id: item.EvaluadorAnteProyecto.id,
              bandera: 'UPDATE',
              nombre:
                item.EvaluadorAnteProyecto.nombre +
                ' ' +
                item.EvaluadorAnteProyecto.apellidos,
              id_relacional: item.id,
            };
            this.UpdateAsignacionProyectoFinal.value.evaluador.push(datos);
          }
        }
      }
      console.log(this.UpdateAsignacionProyectoFinal.value);
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Agregar estudiantes relacionados al proyecto final
  async AgregarEstudianteProyectoFinalEdit() {
    try {
      if (
        this.UpdateAsignacionProyectoFinal.value.estudiante_temporal !== null
      ) {
        if (this.UpdateAsignacionProyectoFinal.value.estudiantes.length < 4) {
          let datos = {
            id: null,
            bandera: 'NUEVO',
            nombre: '',
          };
          var ingreso = 'NO';
          var existe = 'NO';
          for (let info of this.ListaEstudiantes) {
            if (
              info.id ===
              this.UpdateAsignacionProyectoFinal.value.estudiante_temporal
            ) {
              ingreso = 'SI';
              datos = {
                id: info.id,
                bandera: 'NUEVO',
                nombre: info.nombre + ' ' + info.apellidos,
              };
            }
          }
          for (let data of this.UpdateAsignacionProyectoFinal.value
            .estudiantes) {
            if (
              data.id ===
              this.UpdateAsignacionProyectoFinal.value.estudiante_temporal
            ) {
              existe = 'SI';
            }
          }
          if (existe !== 'SI') {
            if (ingreso === 'SI') {
              console.log(this.UpdateAsignacionProyectoFinal.value);
              this.UpdateAsignacionProyectoFinal.value.estudiantes.push(datos);
            } else {
              this.alerta.error('No fue posible agregar el estudiante');
            }
          } else {
            this.alerta.info(
              'El estudiante ya se encuentra en la lista seleccionada.'
            );
          }
        } else {
          this.alerta.error(
            'El numero maximo de estudiantes a cargo de un proyecto es de 4'
          );
        }
      } else {
        this.alerta.error('Por favor seleccione un estudiante valido');
      }
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Eliminar el Estudiante temporalmente
  EliminarTemporalEstudianteFinal1(item: any) {
    var i = this.UpdateAsignacionProyectoFinal.value.estudiantes.indexOf(item);
    this.UpdateAsignacionProyectoFinal.value.estudiantes.splice(i, 1);
  }
  // Elminar el estudiante del ante proyecto
  EliminarTemporalEstudianteFinal2(data) {
    try {
      this.administrador.DeleteEstudianteAnteProyectos(data).subscribe(
        (resp) => {
          var datos: any = resp;
          this.alerta.exito(datos.resp.msg);
          var i =
            this.UpdateAsignacionProyectoFinal.value.estudiantes.indexOf(data);
          this.UpdateAsignacionProyectoFinal.value.estudiantes.splice(i, 1);
          this.CargarDatos();
        },
        (err: any) => {
          if (!err.error.msg) {
            const errores = err.error.error.map((error: any) => {
              return error.msg;
            });
            this.alerta.error(errores.toString());
          } else {
            this.alerta.error(err.error.msg);
          }
        }
      );
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Agregar un director de ante proyecto a uno ya almacenado
  AgregarDirectorProyectoFinalEdit() {
    try {
      if (this.UpdateAsignacionProyectoFinal.value.director_temporal !== null) {
        if (this.UpdateAsignacionProyectoFinal.value.director.length === 0) {
          let datos = {
            id: null,
            bandera: 'NUEVO',
            nombre: '',
          };
          var ingreso = 'NO';
          var existe = 'NO';
          var isEvaluador = 'NO';
          for (let info of this.ListaDirectoresProyectos) {
            if (
              info.id ===
              this.UpdateAsignacionProyectoFinal.value.director_temporal
            ) {
              ingreso = 'SI';
              datos = {
                id: info.id,
                bandera: 'NUEVO',
                nombre: info.nombre + ' ' + info.apellidos,
              };
            }
          }
          if (this.UpdateAsignacionProyectoFinal.value.evaluador.length > 0) {
            for (let data of this.UpdateAsignacionProyectoFinal.value
              .evaluador) {
              if (
                data.id ===
                this.UpdateAsignacionProyectoFinal.value.director_temporal
              ) {
                isEvaluador = 'SI';
              }
            }
          }
          for (let data of this.UpdateAsignacionProyectoFinal.value.director) {
            if (
              data.id ===
              this.UpdateAsignacionProyectoFinal.value.director_temporal
            ) {
              existe = 'SI';
            }
          }
          if (existe !== 'SI') {
            if (ingreso === 'SI') {
              if (isEvaluador === 'NO') {
                this.UpdateAsignacionProyectoFinal.value.director.push(datos);
              } else {
                this.alerta.error(
                  'El director no puede ser evaluador de proyecto'
                );
              }
            } else {
              this.alerta.error('No fue posible agregar el director');
            }
          } else {
            this.alerta.info(
              'El director ya se encuentra en la lista seleccionada.'
            );
          }
        } else {
          this.alerta.error(
            'No se debe seleccionar mas de 1 director de proyecto, para cambiarlo por favor elimine el que tiene asociado'
          );
        }
      } else {
        this.alerta.error('Por favor seleccione un director valido');
      }
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Eliminar de la base de datos el director de proyecto de grado asignado
  EliminarTemporalDirectorProyectoEdit(data) {
    try {
      this.administrador.DeleteDirectorProyectosFinal(data).subscribe(
        (resp) => {
          var datos: any = resp;
          this.alerta.exito(datos.resp.msg);
          console.log(datos);
          var i =
            this.UpdateAsignacionProyectoFinal.value.director.indexOf(data);
          this.UpdateAsignacionProyectoFinal.value.director.splice(i, 1);
          this.CargarDatos();
        },
        (err: any) => {
          if (!err.error.msg) {
            const errores = err.error.error.map((error: any) => {
              return error.msg;
            });
            this.alerta.error(errores.toString());
          } else {
            this.alerta.error(err.error.msg);
          }
        }
      );
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Eliminar el director de ante proyecto temporal
  EliminarTemporalDirectorFinalEdit2(data) {
    try {
      var i = this.UpdateAsignacionProyectoFinal.value.director.indexOf(data);
      this.UpdateAsignacionProyectoFinal.value.director.splice(i, 1);
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Agregar un nuevo evaluador al ante proyecto ya almacenado
  AgregarEvaluadoresAnteProyectoFinalEdit() {
    try {
      if (this.UpdateAsignacionProyectoFinal.value.evaluador_temporal !== null) {
        if (this.UpdateAsignacionProyectoFinal.value.evaluador.length < 4) {
          let datos = {
            id: null,
            bandera: 'NUEVO',
            nombre: '',
          };
          var ingreso = 'NO';
          var existe = 'NO';
          var isDirector = 'NO';
          for (let info of this.ListaEvaluadoresProyectos) {
            if (
              info.id ===
              this.UpdateAsignacionProyectoFinal.value.evaluador_temporal
            ) {
              ingreso = 'SI';
              datos = {
                id: info.id,
                bandera: 'NUEVO',
                nombre: info.nombre + ' ' + info.apellidos,
              };
            }
          }
          if (this.UpdateAsignacionProyectoFinal.value.director.length > 0) {
            for (let data of this.UpdateAsignacionProyectoFinal.value.director) {
              if (
                data.id ===
                this.UpdateAsignacionProyectoFinal.value.evaluador_temporal
              ) {
                isDirector = 'SI';
              }
            }
          }
          for (let data of this.UpdateAsignacionProyectoFinal.value.evaluador) {
            if (
              data.id ===
              this.UpdateAsignacionProyectoFinal.value.evaluador_temporal
            ) {
              existe = 'SI';
            }
          }
          if (existe !== 'SI') {
            if (ingreso === 'SI') {
              if (isDirector === 'NO') {
                this.UpdateAsignacionProyectoFinal.value.evaluador.push(datos);
              } else {
                this.alerta.error(
                  'El evaluador no puede ser director de proyecto'
                );
              }
            } else {
              this.alerta.error('No fue posible agregar el evaluador');
            }
          } else {
            this.alerta.info(
              'El evaluador ya se encuentra en la lista seleccionada.'
            );
          }
        } else {
          this.alerta.error(
            'No se debe seleccionar mas de 3 evaluadores de proyecto'
          );
        }
      } else {
        this.alerta.error('Por favor seleccione un evaluador valido');
      }
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Eliminar un evaluador ya almacenado en la base de datos
  EliminarTemporalEvaluadorFinalEdit(data) {
    try {
      if (this.UpdateAsignacionProyectoFinal.value.evaluador.length > 1) {
        this.administrador.DeleteEvaluadorProyectosFinal(data).subscribe(
          (resp) => {
            var datos: any = resp;
            this.alerta.exito(datos.resp.msg);
            console.log(datos);
            var i =
              this.UpdateAsignacionProyectoFinal.value.evaluador.indexOf(data);
            this.UpdateAsignacionProyectoFinal.value.evaluador.splice(i, 1);
            this.CargarDatos();
          },
          (err: any) => {
            if (!err.error.msg) {
              const errores = err.error.error.map((error: any) => {
                return error.msg;
              });
              this.alerta.error(errores.toString());
            } else {
              this.alerta.error(err.error.msg);
            }
          }
        );
      } else {
        this.alerta.error('Se debe mantener almenos 1 evaluador');
      }
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Eliminar un evaluador en temporal
  EliminarTemporalEvaluadorFinalEdit2(data) {
    try {
      var i = this.UpdateAsignacionProyectoFinal.value.evaluador.indexOf(data);
      this.UpdateAsignacionProyectoFinal.value.evaluador.splice(i, 1);
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Actualizar  proyecto final
  UpdateProyectoFfinal() {
    try {
      if (this.UpdateAsignacionProyectoFinal.invalid) {
        this.alerta.error(
          'Formulario invalido por favor verifique que todos los datos esten ingresados'
        );
      } else {
        if (
          this.UpdateAsignacionProyectoFinal.value.estudiantes.length > 0 &&
          this.UpdateAsignacionProyectoFinal.value.estudiantes.length < 4
        ) {
          if (this.UpdateAsignacionProyectoFinal.value.director.length === 1) {
            if (
              this.UpdateAsignacionProyectoFinal.value.evaluador.length > 0 &&
              this.UpdateAsignacionProyectoFinal.value.evaluador.length < 4
            ) {
              this.administrador
                .UpdateProyectosFinal(this.UpdateAsignacionProyectoFinal.value)
                .subscribe(
                  (resp) => {
                    var data: any = resp;
                    this.alerta.exito(data.resp.msg);
                    this.CargarDatos();
                    console.log(data);
                  },
                  (err: any) => {
                    console.log(err);
                    if (!err.error.msg) {
                      const errores = err.error.error.map((error: any) => {
                        return error.msg;
                      });
                      this.alerta.error(errores.toString());
                    } else {
                      this.alerta.error(err.error.msg);
                    }
                  }
                );
            } else {
              this.alerta.error('Se debe seleccionar mas de 1 evaluador');
            }
          } else {
            this.alerta.error(
              'Se debe seleccionar 1 director de ante proyecto'
            );
          }
        } else {
          this.alerta.error(
            'Se debe seleccionar al menos 1 estudiante a cargo del ante proyecto'
          );
        }
      }
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
}
