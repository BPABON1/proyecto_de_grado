import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignacionProjectsComponent } from './asignacion-projects.component';

describe('AsignacionProjectsComponent', () => {
  let component: AsignacionProjectsComponent;
  let fixture: ComponentFixture<AsignacionProjectsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsignacionProjectsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignacionProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
