import { Component, OnInit,ElementRef,ViewChild } from '@angular/core';
import { AlertasService } from 'src/app/service/alertas.service';
import { LoginService } from 'src/app/service/login.service';
import { FormBuilder, Validators } from '@angular/forms';
import {AdministradorService} from 'src/app/service/administrador.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})

export class UsuariosComponent implements OnInit {
  @ViewChild('closebutton') closebutton:any;
  public pageinaActual: number = 1;
  public filtro = '';
  public hide = true;
  public ListaUsuarios: any = [];
  public ListaRoles: any = [];
  public NewUsuario = this.fb.group({
    identificacion:  [null, Validators.required],
    nombres: ['', Validators.required],
    apellidos: ['', Validators.required],
    id_rol: [null, Validators.required],
    email: ['', Validators.required],
    password: ['', Validators.required],
    director_project: [false],
    evaluador_project: [false],
  });
  public UpdateUsuario = this.fb.group({
    id:  [null, Validators.required],
    identificacion:  [null, Validators.required],
    nombres: ['', Validators.required],
    apellidos: ['', Validators.required],
    id_rol: [null, Validators.required],
    email: ['', Validators.required],
    password: [null],
    estado: ['', Validators.required],
    director_project: [false],
    evaluador_project: [false],
  });
  constructor(
    private alerta: AlertasService,
    private loginservice: LoginService,
    private fb: FormBuilder,
    private administrador: AdministradorService,
    private modalService: NgbModal,
  ) { }

  ngOnInit(): void {
    this.CargarDatos();
  }
  /*
* fecha: 26-03-2022
* autor: branm aldair pabon
* proposito: listar el total de usuarios agregados en el sistema con sus respectivos roles
*/
  async CargarDatos() {
    this.ListaUsuarios = [];
    this.ListaRoles = [];
    this.loginservice.UsuariosRoles()
      .subscribe(resp => {
        var data: any = resp;
        this.ListaRoles = data.resp.roles;
        if (data.resp.usuarios.length > 0) {
          data.resp.usuarios.forEach(function (datos: any) {
          });
          data.resp.usuarios.forEach(function (element: any) {
            element.nombre_completo = element.nombre + ' ' + element.apellidos;
          });
          this.ListaUsuarios = data.resp.usuarios;
        }
        console.log(this.ListaRoles);
      }, (err: any) => {
        if (!err.error.msg) {
          const errores = err.error.error.map((error:any) => { return error.msg });
          this.alerta.error(errores.toString());
        }else{
          this.alerta.error(err.msg);
        }
      });
  }
  /*
* proposito: abrir el modal que corresponde a la creacion del usuario
*/
  AbrirModal() {
    //this.modalService.open(content, { size: 'xl', windowClass: 'modal-xl' });
    this.NewUsuario.patchValue({
      identificacion: null,
      nombres:'',
      apellidos:'',
      id_rol: null,
      email:'',
      password:'',
    });
  }
  /*
  * PROPOSITO: Abrir un modal con la informacion del usuario lista para ser editada
  */
  EditarUsuario(datos:any){
    try {
      console.log(datos);
      let data = {
        is_director: false,
        is_evaluador: false,
      }
      if(datos.director_project === 'SI'){
        data.is_director = true;
      }
      if(datos.evaluador_project === 'SI'){
        data.is_evaluador = true;
      }
      this.UpdateUsuario.patchValue({
        id: datos.id,
        identificacion: datos.identificacion,
        nombres: datos.nombre,
        apellidos: datos.apellidos,
        id_rol: datos.id_rol,
        email: datos.email,
        estado: datos.estado,
        director_project: data.is_director,
        evaluador_project:data.is_evaluador,
      });
    } catch (error:any) {
      this.alerta.error(error.message);
    }
  }
  // Registrar el usuario en la base de datos
  RegistrarUsusario(){
    try {
      if(this.NewUsuario.invalid){
        this.alerta.info("Formulario incompleto por favor ingrese todos los datos solicitados.");
        this.NewUsuario.markAllAsTouched();
        console.log(this.NewUsuario.value);
      }else{
        let projects = {
          is_director: null,
          is_evaluador: null,
        }
        if(this.NewUsuario.value.director_project === true){
          projects.is_director = 'SI';
        }
        if(this.NewUsuario.value.evaluador_project === true){
          projects.is_evaluador = 'SI';
        }
        const datos = {
          ...this.NewUsuario.value,
          is_director: projects.is_director,
          is_evaluador: projects.is_evaluador,
        }
        this.administrador.RegistrerUser(datos).subscribe(resp => {
          var data:any = resp;
          console.log(data);
          // Mesaje de exitoso
          this.alerta.exito(data.resp.msg);
          // Llamar funcion de datos registrados
          this.CargarDatos();
        }, (err) => {
          console.log(err);
          // Mensajes de error
          if (!err.error.msg) {
            const errores = err.error.error.map((error:any) => { return error.msg });
            this.alerta.error(errores.toString());
          }else{
            this.alerta.error(err.error.msg);
          }
        });
      }
    } catch (error:any) {
      this.alerta.error(error.message);
    }
  }
  // Actualizar los datos del usuario seleccionado
  async UpdateUser(){
    try {
      if(this.UpdateUsuario.invalid){
        this.alerta.info("Formulario incompleto por favor ingrese todos los datos solicitados.");
        this.UpdateUsuario.markAllAsTouched();
      }else{
        let projects = {
          is_director: null,
          is_evaluador: null,
        }
        if(this.UpdateUsuario.value.director_project === true){
          projects.is_director = 'SI';
        }
        if(this.UpdateUsuario.value.evaluador_project === true){
          projects.is_evaluador = 'SI';
        }
        const datos = {
          ...this.UpdateUsuario.value,
          is_director: projects.is_director,
          is_evaluador: projects.is_evaluador,
        }
        this.administrador.UpdateUser(datos).subscribe(resp => {
          var data:any = resp;
          console.log(data);
          this.alerta.exito(data.resp.msg);
          this.CargarDatos();
        }, (err) => {
          console.log(err);
          if (!err.error.msg) {
            const errores = err.error.error.map((error:any) => { return error.msg });
            this.alerta.error(errores.toString());
          }else{
            this.alerta.error(err.error.msg);
          }
        });
      }
    } catch (error:any) {
      this.alerta.error(error.message);
    }
  }
}
