import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsuariosComponent } from './usuarios.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgxPaginationModule } from 'ngx-pagination'; // At the top of your module


describe('UsuariosComponent', () => {
  let component: UsuariosComponent;
  let fixture: ComponentFixture<UsuariosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
        NgxPaginationModule,
        HttpClientTestingModule
      ],
      declarations: [ UsuariosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Creacion Componente', () => {
    const fixture = TestBed.createComponent(UsuariosComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
  it('validacion de formulario reactivo para creacion de usuario', () => {
    // const formElement = fixture.debugElement.nativeElement.querySelector('#NewUsuario');
    // const inputElement = formElement.querySelectorAll('input');
    // expect(inputElement.length).toEqual(2);
    });
  it('Validacion de campos de formulario de nuevo usuario', () => {
    // const login = component.NewUsuario;
    // const loginValores ={
    //   identificacion:  null,
    //   nombres: '',
    //   apellidos: '',
    //   id_rol: null,
    //   email: '',
    //   password: '',
    // }
    // expect(login.value).toEqual(loginValores);
    });
});
