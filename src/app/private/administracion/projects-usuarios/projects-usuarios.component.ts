import { Component, OnInit } from '@angular/core';
import { AlertasService } from 'src/app/service/alertas.service';
import { LoginService } from 'src/app/service/login.service';
import { FormBuilder, Validators,FormControl } from '@angular/forms';
import {AdministradorService} from 'src/app/service/administrador.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
@Component({
  selector: 'app-projects-usuarios',
  templateUrl: './projects-usuarios.component.html',
  styleUrls: ['./projects-usuarios.component.css']
})
export class ProjectsUsuariosComponent implements OnInit {
  public pageinaActual: number = 1;
  public filtro = '';
  public hide = true;
  public ListaEstudiantes: any = [];
  public ListaProjects: any = [];
  public myControl = new FormControl();
  public NewProjects = this.fb.group({
    titulo:  ['', Validators.required],
    // estudiantes: this.fb.array([]),
    // estudiante_temporal: null,
    descripcion: '',
  });
  public UpdateProjects = this.fb.group({
    id:  [null, Validators.required],
    titulo:  ['', Validators.required],
    // estudiantes: this.fb.array([]),
    // estudiante_temporal: null,
    estado: ['', Validators.required],
    descripcion: '',
  });
  constructor(
    private alerta: AlertasService,
    private fb: FormBuilder,
    private administrador: AdministradorService,
  ) {}
  ngOnInit(): void {
    this.CargarDatos();
  }
  // Listar el total de usuarios y almacenar los usuarios con el Rol de estudiante
  async CargarDatos() {
    // this.ListaEstudiantes = [];
    this.ListaProjects = [];
    this.administrador.ProjectsEstudiantes()
      .subscribe(resp => {
        var data: any = resp;
        /* if (data.resp.usuarios.length > 0) {
          data.resp.usuarios.forEach(function (element: any) {
            element.nombre_completo = element.nombre + ' ' + element.apellidos;
          });
          this.ListaEstudiantes = data.resp.usuarios;
        } */
        if(data.resp.projects.length > 0){
          this.ListaProjects = data.resp.projects;
        }
        // console.log(this.ListaEstudiantes);
      }, (err: any) => {
        if (!err.error.msg) {
          const errores = err.error.error.map((error:any) => { return error.msg });
          this.alerta.error(errores.toString());
        }else{
          this.alerta.error(err.error.msg);
        }
      });
  }
  // Guardar el proyecto y los estudiantes
  RegistrarProjects(){
    try {
      // validar que el formulario este registrado correctamente
      if(this.NewProjects.invalid){
        this.alerta.info("Formulario incompleto por favor ingrese todos los datos solicitados.");
        this.NewProjects.markAllAsTouched();
      }else{
        // validar que exista mas de 1 usuario en el proyeco y menos de 3
        // if(this.NewProjects.value.estudiantes.length > 0 && this.NewProjects.value.estudiantes.length <= 3){
          this.administrador.RegisterProjects(this.NewProjects.value).subscribe(resp => {
            var data:any = resp;
            console.log(data);
            // Mensaje de exitoso y recarga de datos
            this.alerta.exito(data.resp.msg);
            this.CargarDatos();
            this.ClearNewProjects();
          }, (err) => {
            // Alerta de mensajes de error
            console.log(err);
            if (!err.error.msg) {
              const errores = err.error.error.map((error:any) => { return error.msg });
              this.alerta.error(errores.toString());
            }else{
              this.alerta.error(err.error.msg);
            }
          });
          console.log(this.NewProjects.value);
        /*}else{
          this.alerta.info("Para agregar el proyecto se debe asociar almenos 1 estudiante.");
        } */
      }
    } catch (error:any) {
      this.alerta.error(error.message);
    }
  }
  // Agregar el estudiante a la lista que realizara el proyecto de grado
  AgregarEstudiante(){
    try {
      let datos = {
        id:null,
        nombre_completo: '',
      };
      var ingreso = 'NO';
      var existe = 'NO';
      for(let info of this.ListaEstudiantes){
        if(info.id === this.NewProjects.value.estudiante_temporal){
          ingreso = 'SI';
          datos = {
            id:info.id,
            nombre_completo: info.nombre_completo,
          };
        }
      }
      for(let data of this.NewProjects.value.estudiantes){
        if(data.id === this.NewProjects.value.estudiante_temporal){
          existe = 'SI';
        }
      }
      if(existe !== 'SI'){
        if(ingreso === 'SI'){
          this.NewProjects.value.estudiantes.push(datos);
          console.log(this.NewProjects.value)
        }else{
          this.alerta.error('Ocurrio un problema inesperado.');
        }
      }else{
        this.alerta.info('El estudiante '+datos.nombre_completo+' ya se encuentra en la lista seleccionada.');
      }
    } catch (error:any) {
      this.alerta.error(error.message);
    }
  }
  // Eliminar el estudiante temporalmente
  EliminarTemporal(item:any) {
    var i = this.NewProjects.value.estudiantes.indexOf(item);
    this.NewProjects.value.estudiantes.splice(i, 1);
  }
  // Eliminar el estudiante temporalmente
  EliminarTemporalUpate(item:any) {
    // var i = this.UpdateProjects.value.estudiantes.indexOf(item);
    // this.UpdateProjects.value.estudiantes.splice(i, 1);
  }
  // Almacenar la informacion temporal del proyecto que se desea actualizar el proyecto
  EditarProject(datos:any){
    try {
      console.log(this.UpdateProjects);
      /* for(let item of this.UpdateProjects.value.estudiantes){
        var i = this.UpdateProjects.value.estudiantes.indexOf(item);
        this.UpdateProjects.value.estudiantes.splice(i, 1);
      } */
      this.UpdateProjects.patchValue({
        id: datos.id,
        titulo: datos.titulo,
        estado: datos.estado,
        descripcion: datos.descripccion,
      });
      /* for(let info of datos.StudentProjects){
        var datos:any = {
          id_relacion: null,
          id_estudiante: null,
          nombre_completo: '',
        }
        if(info.Usuario){
          datos = {
            id_relacion: info.id,
            id_estudiante: info.id_estudiante,
            nombre_completo: info.Usuario.nombre+' '+info.Usuario.apellidos,
            bandera: 'ANTIGUO'
          }
          this.UpdateProjects.value.estudiantes.push(datos);
        }
      } */
    } catch (error:any) {
      this.alerta.error(error.message);
    }
  }
  // Actualizar la informacion del proyecto de los estudiantes
  UpdateServerProjects(){
    try {
      if(this.UpdateProjects.invalid){
        this.alerta.info("Formulario incompleto por favor ingrese todos los datos solicitados.");
        this.UpdateProjects.markAllAsTouched();
      }else{
        // if(this.UpdateProjects.value.estudiantes.length > 0){
          this.administrador.UpdateProjects(this.UpdateProjects.value).subscribe(resp => {
            var data:any = resp;
            console.log(data);
            this.alerta.exito(data.resp.msg);
            this.CargarDatos();
          }, (err) => {
            if (!err.error.msg) {
              const errores = err.error.error.map((error:any) => { return error.msg });
              this.alerta.error(errores.toString());
            }else{
              this.alerta.error(err.error.msg);
            }
          });
        /*}else{
          this.alerta.info("Para actualizar el proyecto se debe asociar almenos 1 estudiante.");
        } */
      }
    } catch (error:any) {
      this.alerta.error(error.message);
    }
  }
  // Agregar estudiantes nuevos al proyecto ya creado anteriormente
  /*AgregarEstudianteUpdate(){
    try {
      let datos = {
        id_relacion: null,
        id_estudiante: null,
        nombre_completo: '',
        bandera: 'NUEVO'
      };
      var ingreso = 'NO';
      var existe = 'NO';
      for(let info of this.ListaEstudiantes){
        if(info.id === this.UpdateProjects.value.estudiante_temporal){
          ingreso = 'SI';
          datos = {
            id_relacion: null,
            id_estudiante:info.id,
            nombre_completo: info.nombre_completo,
            bandera: 'NUEVO'
          };
        }
      }
      for(let data of this.UpdateProjects.value.estudiantes){
        if(data.id_estudiante === this.UpdateProjects.value.estudiante_temporal){
          existe = 'SI';
        }
      }
      if(existe !== 'SI'){
        if(ingreso === 'SI'){
          this.UpdateProjects.value.estudiantes.push(datos);
          console.log(this.UpdateProjects.value)
        }else{
          this.alerta.error('Ocurrio un problema inesperado.');
        }
      }else{
        this.alerta.info('El estudiante '+datos.nombre_completo+' ya se encuentra en la lista seleccionada.');
      }
    } catch (error:any) {
      this.alerta.error(error.message);
    }
  }  */
  // Eliminar la asociacion del estudiante seleccionado en la base de datos
  /* EliminarServer(datos:any){
    try {
      let antiguos:any=[];
      for(let data of this.UpdateProjects.value.estudiantes){
        if(data.bandera === 'ANTIGUO'){
          antiguos.push(data);
        }
      }
      if(antiguos.length >= 2){
        this.administrador.DeleteProjects(datos).subscribe(resp => {
          var data:any = resp;
          this.alerta.exito(data.resp.msg);
          this.CargarDatos();
          this.EliminarTemporalUpate(datos);
        }, (err) => {
          if (!err.error.msg) {
            const errores = err.error.error.map((error:any) => { return error.msg });
            this.alerta.error(errores.toString());
          }else{
            this.alerta.error(err.error.msg);
          }
        });
      }else{
        this.alerta.info('Para poder eliminar el estudiante debe tener mínimo otro estudiante a cargo del proyecto(por favor primero agregue el otro estudiante y después si elimine el que desea quitar)');
      }
    } catch (error:any) {
      this.alerta.error(error.message);
    }
  } */
  // Limpiar el formulario de nuevos proyectos
  ClearNewProjects(){
    try {
      /* for(let item of this.NewProjects.value.estudiantes){
        var i = this.NewProjects.value.estudiantes.indexOf(item);
        this.NewProjects.value.estudiantes.splice(i, 1);
      } */
      this.NewProjects.patchValue({
        titulo: '',
        descripcion: '',
      })
    } catch (error:any) {
      this.alerta.error(error.message);
    }
  }
    // Limpiar el formulario de actualizar proyectos
  async ClearUpdateProjects(datos:any){
    try {
      /* for(let item of this.UpdateProjects.value.estudiantes){
        var i = this.UpdateProjects.value.estudiantes.indexOf(item);
        this.UpdateProjects.value.estudiantes.splice(i, 1);
      } */
      console.log(datos);
      this.UpdateProjects.patchValue({
        id: null,
        titulo: '',
        // estudiante_temporal: null,
        estado: '',
        descripcion: '',
      });
      this.EditarProject(datos);
    } catch (error:any) {
      this.alerta.error(error.message);
    }
  }
}
