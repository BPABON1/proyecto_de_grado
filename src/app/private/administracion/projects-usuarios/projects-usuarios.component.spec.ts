import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsUsuariosComponent } from './projects-usuarios.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule} from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ProjectsUsuariosComponent', () => {
  let component: ProjectsUsuariosComponent;
  let fixture: ComponentFixture<ProjectsUsuariosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule,
        ReactiveFormsModule,
        HttpClientTestingModule
      ],
      declarations: [ ProjectsUsuariosComponent ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Inicio de componente', () => {
    const fixture = TestBed.createComponent(ProjectsUsuariosComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
  it('validacion de formulario reactivo para creacion de proyecto y asignacion de usuarios', () => {
    const formElement = fixture.debugElement.nativeElement.querySelector('#NewProjects');
    const inputElement = formElement.querySelectorAll('input');
    expect(inputElement.length).toEqual(2);
    });
  it('Validacion de campos de formulario de nuevo proyecto', () => {
    const fixture = TestBed.createComponent(ProjectsUsuariosComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
     const login = component.NewProjects;
     const loginValores ={
      titulo: '',
      estudiantes: null,
      estudiante_temporal: null,
       }
     expect(login.value).toEqual(loginValores);
    });
});
