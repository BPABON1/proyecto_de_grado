import { Routes, RouterModule, RouterLink } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component';
import {NotFountComponent} from '../public/not-fount/not-fount.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {UsuariosComponent} from './administracion/usuarios/usuarios.component';
import {ProjectsUsuariosComponent} from './administracion/projects-usuarios/projects-usuarios.component';
import {EntregarComponent} from './proyectos/entregar/entregar.component';
import {ListaEvaluarComponent} from './proyectos/lista-evaluar/lista-evaluar.component';
import {ShowEvaluarComponent} from './proyectos/show-evaluar/show-evaluar.component';
import {PublicarComponent} from './proyectos/publicar/publicar.component';
import {ListarBuscadorComponent} from './proyectos/listar-buscador/listar-buscador.component';
import {DetalleBuscadorComponent} from './proyectos/detalle-buscador/detalle-buscador.component';
import { AsignacionProjectsComponent } from './administracion/asignacion-projects/asignacion-projects.component';
import { TiposEntregasComponent } from './proyectos/tipos-entregas/tipos-entregas.component';
import { EntregarAnteProyectoComponent } from './proyectos/entregar-ante-proyecto/entregar-ante-proyecto.component';
import { TipoEvaluacionComponent } from './proyectos/tipo-evaluacion/tipo-evaluacion.component';
import { EvaluarAnteProyectoComponent } from './proyectos/evaluar-ante-proyecto/evaluar-ante-proyecto.component';
import { ListaEvaluarAnteProyectoComponent } from './proyectos/lista-evaluar-ante-proyecto/lista-evaluar-ante-proyecto.component';
const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      { path: '', component: DashboardComponent, data: { titulo: 'Inicio' } },
      { path: 'administracion/usuarios', component: UsuariosComponent, data: { titulo: 'Usuarios' } },
      { path: 'administracion/projects-usuarios', component: ProjectsUsuariosComponent, data: { titulo: 'Lista de proyectos' } },
      { path: 'proyectos/entregar', component: EntregarComponent, data: { titulo: 'Entregar proyecto de grado' } },
      { path: 'proyectos/lista-evaluar', component: ListaEvaluarComponent, data: { titulo: 'Lista evaluar proyecto de grado' } },
      { path: 'proyectos/show-evaluar/:id', component: ShowEvaluarComponent, data: { titulo: 'Evaluar proyecto de grado' } },
      { path: 'proyectos/publicar', component: PublicarComponent, data: { titulo: 'Publicar proyectos de grado' } },
      { path: 'proyectos/listar-buscador', component: ListarBuscadorComponent, data: { titulo: 'Lista de proyectos públicos' } },
      { path: 'proyectos/detalle-buscador/:id', component: DetalleBuscadorComponent, data: { titulo: 'Detalle del proyecto publico ' } },
      { path: 'administracion/asignacion-projects', component: AsignacionProjectsComponent, data: { titulo: 'Asignación de proyectos' } },
      { path: 'proyectos/tipos-entregas', component: TiposEntregasComponent, data: { titulo: 'Seleccionar proyectos, ha entregar' } },
      { path: 'proyectos/entregar-ante-proyecto', component: EntregarAnteProyectoComponent, data: { titulo: 'Entregar ante proyecto' } },
      { path: 'proyectos/tipo-evaluacion', component: TipoEvaluacionComponent, data: { titulo: 'Seleccionar tipo de evaluacion' } },
      { path: 'proyectos/evaluar-ante-proyecto/:id', component: EvaluarAnteProyectoComponent, data: { titulo: 'Evaluar ante proyecto' } },
      { path: 'proyectos/lista-evaluar-ante-proyecto', component: ListaEvaluarAnteProyectoComponent, data: { titulo: 'Lista evaluar ante proyecto' } },

      { path: '**', component: NotFountComponent },
    ]
  }];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrivateRoutingModule { }
