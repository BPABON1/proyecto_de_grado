import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EntregarComponent } from './entregar.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgxPaginationModule } from 'ngx-pagination'; // At the top of your module
describe('EntregarComponent', () => {
  let component: EntregarComponent;
  let fixture: ComponentFixture<EntregarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
        NgxPaginationModule,
        HttpClientTestingModule
      ],
      declarations: [ EntregarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EntregarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Creacion de componente', () => {
    expect(component).toBeTruthy();
  });
  it('validacion de formulario reactivo para entrega de proyecto', () => {
    const formElement = fixture.debugElement.nativeElement.querySelector('#NewForm');
    const inputElement = formElement.querySelectorAll('input');
    expect(inputElement.length).toEqual(2);
    });
  it('Validacion de inicio de valores de campos de formulario reactivo', () => {
    const fixture = TestBed.createComponent(EntregarComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
     const login = component.NewForm;
     const loginValores ={
        id_project: null,
        titulo: null,
        resumen: '',
        conclucion:'',
        documento:'',
       }
     expect(login.value).toEqual(loginValores);
    });
});
