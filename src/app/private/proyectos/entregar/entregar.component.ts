import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { AlertasService } from 'src/app/service/alertas.service';
import { AdministradorService } from 'src/app/service/administrador.service';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-entregar',
  templateUrl: './entregar.component.html',
  styleUrls: ['./entregar.component.css'],
})
export class EntregarComponent implements OnInit {
  @ViewChild('inputFile') myInputVariable: ElementRef;
  public SubirTrabajo: string = 'NO';
  public file: File = null;
  public selectedFiles: any;
  public InfoTemporal: any;
  public pageinaActual: number = 1;
  public NewForm = this.fb.group({
    id_project: [null, Validators.required],
    titulo: [null, Validators.required],
    observaciones: ['', Validators.required],
    documento: ['', Validators.required],
  });
  public Temporal = this.fb.group({
    descripcion: '',
    estado: '',
    fecha_creacion: '',
    fecha_modificacion: '',
    resumen: '',
    url_documento: '',
  });
  public historial: any;
  public NotasHistorial: any[] = [];
  constructor(
    private route: Router,
    private fb: FormBuilder,
    private alerta: AlertasService,
    private administrador: AdministradorService
  ) {}

  ngOnInit(): void {
    this.CargarDatos();
  }
  // Listar el total de usuarios y almacenar los usuarios con el Rol de estudiante
  async CargarDatos() {
    this.administrador.ShowSelectProjectsFinal().subscribe(
      (resp) => {
        var data: any = resp;
        this.SubirTrabajo = 'NO';
        if (data.resp.historial !== null) {
          data.resp.historial.DocumentoProyectoProyectoFinal.forEach((element) => {
            if (element.estado === null) {
              element.estado = 'Pendiente';
            }
          });
          this.historial = data.resp.historial;
          if (
            data.resp.historial.estado_aprobacion === 'CORREGIR' ||
            data.resp.historial.estado_aprobacion === null
          ) {
            this.SubirTrabajo = 'SI';
          }
        } else {
          this.SubirTrabajo = 'SI';
        }
        if (data.resp.info !== null) {
          this.NewForm.patchValue({
            id_project: data.resp.info.id,
            titulo: data.resp.info.titulo,
          });
        } else {
          this.alerta.error(
            'El estudiante no cuenta con proyectos asignados para subir'
          );
          this.route.navigateByUrl('/home');
        }
        console.log(this.historial);
        // console.log(data);
      },
      (err: any) => {
        if (!err.error.msg) {
          const errores = err.error.error.map((error: any) => {
            return error.msg;
          });
          this.alerta.error(errores.toString());
        } else {
          this.alerta.error(err.error.msg);
          this.route.navigateByUrl('/home');
        }
      }
    );
  }
  // Seleccionar el archivo PDF a subir en la plataforma web
  selectFile(event: any) {
    this.file = event.target.files[0];
    if (
      event.target.files[0].name !== null &&
      event.target.files[0].name !== ''
    ) {
      this.NewForm.patchValue({
        documento: event.target.files[0].name,
      });
    } else {
      this.NewForm.patchValue({
        documento: null,
      });
    }
    this.selectedFiles = this.file.name;
    console.log(event);
  }
  // Abrir el modal de informacion para ver las observaciones del evaluador y la informacion de proyecto subido
  VerProjects(data) {
    try {
      let NuevaRuta = data.url_documento.replace(/\\/g, '/');
      console.log(NuevaRuta);
      const search = 'C:/xampp/htdocs';
      const replacer = new RegExp(search, 'g');
      let Temporal = NuevaRuta.replace(replacer, 'http://localhost');
      console.log(Temporal);
      // window.open(Temporal, 'explorer');
      this.Temporal.patchValue({
        descripcion: data.descripcion,
        estado: data.estado,
        fecha_creacion: data.fecha_creacion,
        fecha_modificacion: data.fecha_modificacion,
        url_documento: Temporal,
      });
      this.NotasHistorial = data.NotasProyectosFinal;
      console.log(this.NotasHistorial);
      console.log(this.Temporal.value);
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  VerPdf(url) {
    window.open(url, '_self');
  }
  // Enviar a guardar el proyecto de grado
  UploadProjects() {
    try {
      // validacion de campos obligatorios en formulario
      if (this.NewForm.invalid) {
        this.alerta.info(
          'Formulario incompleto por favor ingrese toda la información solicitada'
        );
      } else {
        // enviar al servidor el pdf en forma binaria
        this.administrador
          .SubirDocumentoProyectoFinal(
            this.file,
            this.NewForm.value.id_project,
            this.NewForm.value.observaciones
          )
          .then((data) => {
            if (data.bandera === 'MAL') {
              this.alerta.error(data.msg);
            } else {
              // alerta de exitoso y datos ingresados correctamente
              this.alerta.exito(data.msg);
              // Recarga de datos
              this.CargarDatos();
            }
          })
          .catch((err) => {
            // Alertas en caso de error 404
            if (!err.error.msg) {
              const errores = err.error.error.map((error: any) => {
                return error.msg;
              });
              this.alerta.error(errores.toString());
            } else {
              this.alerta.error(err.error.msg);
            }
          });
      }
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
}
