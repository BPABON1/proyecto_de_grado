import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicarComponent } from './publicar.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgxPaginationModule } from 'ngx-pagination'; // At the top of your module

describe('PublicarComponent', () => {
  let component: PublicarComponent;
  let fixture: ComponentFixture<PublicarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
        NgxPaginationModule,
        HttpClientTestingModule
      ],
      declarations: [ PublicarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Inicio de componente', () => {
    expect(component).toBeTruthy();
  });
  it('Validacion de tipo de lista', () => {
    expect(component).toBeTruthy();
    });
});
