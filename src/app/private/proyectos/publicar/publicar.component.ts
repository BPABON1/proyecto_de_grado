import { Component, OnInit } from '@angular/core';
import { AlertasService } from 'src/app/service/alertas.service';
import { AdministradorService } from 'src/app/service/administrador.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-publicar',
  templateUrl: './publicar.component.html',
  styleUrls: ['./publicar.component.css'],
})
export class PublicarComponent implements OnInit {
  public pageinaActual: number = 1;
  public filtro = '';
  public hide = true;
  public ListaProjects: any = [];
  constructor(
    private alerta: AlertasService,
    private administrador: AdministradorService
  ) {}

  ngOnInit(): void {
    this.CargarDatos();
  }
  /*
   * proposito: listar el total de proyectos aprobados
   */
  async CargarDatos() {
    this.ListaProjects = [];
    this.administrador.ListarProjectPublicos().subscribe(
      (resp) => {
        var data: any = resp;
        const search = 'C:/xampp/htdocs';
        const replacer = new RegExp(search, 'g');
        for (let item of data.resp.lista) {
          var estado = 'Privado';
          if (item.ProjectsUpload.DocumentosSubidos[0].publicar === 'SI') {
            estado = 'Publico';
          }
          var NuevaRuta =
            item.ProjectsUpload.DocumentosSubidos[0].url_documento.replace(
              /\\/g,
              '/'
            );
          var url = NuevaRuta.replace(replacer, 'http://localhost');
          var datos = {
            titulo: item.titulo,
            id_proyecto_general: item.id,
            id_documento_subido: item.ProjectsUpload.DocumentosSubidos[0].id,
            publico: estado,
            url_documento: url,
            estado: item.ProjectsUpload.estado_aprobacion,
          };
          this.ListaProjects.push(datos);
        }
        console.log(this.ListaProjects);
      },
      (err: any) => {
        if (!err.error.msg) {
          const errores = err.error.error.map((error: any) => {
            return error.msg;
          });
          this.alerta.error(errores.toString());
        } else {
          this.alerta.error(err.error.msg);
        }
      }
    );
  }
  // Cambiar de estado de publicacion del documento a un estado publico para ser visualizado
  RefresStatusProjects(item){
    var mensaje = '';
    let NuevoEstado = '';
    if(item.publico === 'Privado'){
      NuevoEstado = 'Publico';
      mensaje = 'Está seguro de poner público el proyecto de grado  para ser consultado desde el módulo de búsqueda.';
    }else{
      NuevoEstado = 'Privado';
      mensaje = 'Esta seguro de cambiar el estado y dejar Privado el proyecto de grado ';
    }
    // Mensaje de alerta si desea editar el tipo de publicacion de proyecto
    Swal.fire({
      title: mensaje,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SI',
      cancelButtonText: 'NO',
    }).then((result) => {
      if (result.isConfirmed) {
        // Funcion de edicion del estado de proyecto
        this.UpdateStatusPublico(item,NuevoEstado);
        this.alerta.exito('Sesión cerrada de forma correcta ');
      }
    });
  }
  // Actualizar el nuevo estado del proyecto en la base de datos
  UpdateStatusPublico(data,status){
    try {
      let Form = {
        ...data,
        nuevo_estado: status
      }
      this.administrador.UpdateStatusProjects(Form).subscribe(
        (resp) => {
          var data: any = resp;
          this.alerta.exito(data.resp.msg);
          this.CargarDatos();
        },
        (err: any) => {
          if (!err.error.msg) {
            const errores = err.error.error.map((error: any) => {
              return error.msg;
            });
            this.alerta.error(errores.toString());
          } else {
            this.alerta.error(err.error.msg);
          }
        }
      );
      console.log(Form)
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
}
