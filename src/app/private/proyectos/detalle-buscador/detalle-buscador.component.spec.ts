import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleBuscadorComponent } from './detalle-buscador.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('DetalleBuscadorComponent', () => {
  let component: DetalleBuscadorComponent;
  let fixture: ComponentFixture<DetalleBuscadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [ DetalleBuscadorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleBuscadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Creacion Componente', () => {
    const fixture = TestBed.createComponent(DetalleBuscadorComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
  it('Carga de componente', () => {
    const fixture = TestBed.createComponent(DetalleBuscadorComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.div')?.textContent).toContain('Informacio cargada');
  });
});
