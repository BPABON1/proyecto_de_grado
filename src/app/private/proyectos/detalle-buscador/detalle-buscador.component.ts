import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AlertasService } from 'src/app/service/alertas.service';
import {AdministradorService} from 'src/app/service/administrador.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-detalle-buscador',
  templateUrl: './detalle-buscador.component.html',
  styleUrls: ['./detalle-buscador.component.css']
})
export class DetalleBuscadorComponent implements OnInit {
  public id_proyecto:number;
  public estudiantes:any = [];
  public NameDirector: string;
  public titulo:string;
  public FechaCreacion:string;
  public resumen:string;
  public concluciones:string;
  public url:any;
  public Recomendaciones:any = [];
  constructor(
    private administrador: AdministradorService,
    private route: ActivatedRoute,
    private router: Router,
    private alerta: AlertasService,
    public sanitizer: DomSanitizer,
  ) { }

  ngOnInit(): void {
    this.id_proyecto = parseInt(this.route.snapshot.paramMap.get('id'));
    this.CargarDatos();
  }
  /*
   * proposito: listar el total de proyectos aprobados
   */
  async CargarDatos() {
    this.estudiantes = [];
    this.Recomendaciones = [];
    this.administrador.DetalleProyectoPublicado(this.id_proyecto).subscribe(
      (resp) => {
        var data: any = resp;
        this.titulo = data.resp.lista.titulo;
        for(let item of data.resp.estudiante.StudentProjects){
          var datos = {
            nombre: item.Usuario.nombre+' '+item.Usuario.apellidos,
          }
          this.estudiantes.push(datos);
        }
        if(data.resp.estudiante.EvaluadoresAnteProyecto !== null){
          this.NameDirector = data.resp.estudiante.EvaluadoresAnteProyecto[0].NameDirector.nombre +' '+
          data.resp.estudiante.EvaluadoresAnteProyecto[0].NameDirector.apellidos;
        }
        this.FechaCreacion = data.resp.lista.ProjectsUpload.DocumentosSubidos[0].fecha_modificacion;
        this.resumen = data.resp.lista.descripccion;
        this.concluciones = data.resp.lista.ProjectsUpload.DocumentosSubidos[0].conclusion_publica;
        if(data.resp.lista.ProjectsUpload.DocumentosSubidos[0].NotasProyectosFinal.length > 0){
          this.Recomendaciones = data.resp.lista.ProjectsUpload.DocumentosSubidos[0].NotasProyectosFinal.map(item => {return item});
        }
        console.log(this.Recomendaciones);
        let search = 'C:/xampp/htdocs';
        let replacer = new RegExp(search, 'g');
        let Ruta = data.resp.lista.ProjectsUpload.DocumentosSubidos[0].url_documento.replace(/\\/g,'/');
        var NuevaRuta= Ruta.replace(replacer, 'http://localhost');
        this.url  = this.sanitizer.bypassSecurityTrustResourceUrl(NuevaRuta),
        console.log(data);
        console.log(this.url);
      },
      (err: any) => {
        if (!err.error.msg) {
          const errores = err.error.error.map((error: any) => {
            return error.msg;
          });
          this.alerta.error(errores.toString());
        } else {
          this.alerta.error(err.error.msg);
        }
      }
    );
  }
}
