import { Component, OnInit } from '@angular/core';
import { AlertasService } from 'src/app/service/alertas.service';
import { AdministradorService } from 'src/app/service/administrador.service';
@Component({
  selector: 'app-listar-buscador',
  templateUrl: './listar-buscador.component.html',
  styleUrls: ['./listar-buscador.component.css']
})
export class ListarBuscadorComponent implements OnInit {
  public pageinaActual: number = 1;
  public filtro = '';
  public hide = true;
  public ListaProjects: any = [];
  constructor(
    private alerta: AlertasService,
    private administrador: AdministradorService
  ) { }

  ngOnInit(): void {
    this.CargarDatos();
  }
  /*
   * proposito: listar el total de proyectos aprobados
   */
  async CargarDatos() {
    this.ListaProjects = [];
    this.administrador.ListarProjectPublicados().subscribe(
      (resp) => {
        var data: any = resp;
        for (let item of data.resp.lista) {
          var datos = {
            titulo: item.titulo,
            id_proyecto_general: item.id,
            id_documento_subido: item.ProjectsUpload.DocumentosSubidos[0].id,
            resumen:  item.descripccion,
            // conclucion:  item.ProjectsUpload.DocumentosSubidos[0].conclusion_publica,
            estado: item.ProjectsUpload.estado_aprobacion,
          };
          this.ListaProjects.push(datos);
        }
        console.log(data);
      },
      (err: any) => {
        if (!err.error.msg) {
          const errores = err.error.error.map((error: any) => {
            return error.msg;
          });
          this.alerta.error(errores.toString());
        } else {
          this.alerta.error(err.error.msg);
        }
      }
    );
  }
}
