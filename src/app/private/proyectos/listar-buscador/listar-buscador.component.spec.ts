import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarBuscadorComponent } from './listar-buscador.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgxPaginationModule } from 'ngx-pagination'; // At the top of your module

describe('ListarBuscadorComponent', () => {
  let component: ListarBuscadorComponent;
  let fixture: ComponentFixture<ListarBuscadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
        NgxPaginationModule,
        HttpClientTestingModule
      ],
      declarations: [ ListarBuscadorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarBuscadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
