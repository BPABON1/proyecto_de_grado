import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaEvaluarComponent } from './lista-evaluar.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgxPaginationModule } from 'ngx-pagination'; // At the top of your module

describe('ListaEvaluarComponent', () => {
  let component: ListaEvaluarComponent;
  let fixture: ComponentFixture<ListaEvaluarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
        NgxPaginationModule,
        HttpClientTestingModule
      ],
      declarations: [ ListaEvaluarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaEvaluarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
