import { Component, OnInit } from '@angular/core';
import { AlertasService } from 'src/app/service/alertas.service';
import { LoginService } from 'src/app/service/login.service';


@Component({
  selector: 'app-lista-evaluar',
  templateUrl: './lista-evaluar.component.html',
  styleUrls: ['./lista-evaluar.component.css']
})
export class ListaEvaluarComponent implements OnInit {
  public pageinaActual: number = 1;
  public filtro = '';
  public ListaProyectos: any = [];
  constructor(
    private alerta: AlertasService,
    private loginservice: LoginService,
  ) { }

  ngOnInit(): void {
    this.CargarDatos();
  }
  /*
* fecha: 30-10-2022
* autor: branm aldair pabon
* proposito: Listar el total de ante proyectos pendientes por evaluar
*/
async CargarDatos() {
  this.ListaProyectos = [];
  this.loginservice.LitProjectsFinalEvaluar()
    .subscribe(resp => {
      var data: any = resp;
      this.ListaProyectos = data.resp.lista;
    }, (err) => {
      if (!err.error.msg) {
        const errores = err.error.error.map((error:any) => { return error.msg });
        this.alerta.error(errores.toString());
      }else{
        this.alerta.error(err.error.msg);
      }
    });
}
}
