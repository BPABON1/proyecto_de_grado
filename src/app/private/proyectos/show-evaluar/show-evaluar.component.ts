import { Component, OnInit } from '@angular/core';
import { AlertasService } from 'src/app/service/alertas.service';
import { LoginService } from 'src/app/service/login.service';
import { FormBuilder, Validators } from '@angular/forms';
import {AdministradorService} from 'src/app/service/administrador.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
@Component({
  selector: 'app-show-evaluar',
  templateUrl: './show-evaluar.component.html',
  styleUrls: ['./show-evaluar.component.css']
})
export class ShowEvaluarComponent implements OnInit {
  public pageinaActual: number = 1;
  public filtro = '';
  public Filter = '';
  public id_proyecto: number;
  public NewForm = this.fb.group({
    id_project: [null, Validators.required],
    titulo: [null, Validators.required],
    descripcion: ['', Validators.required],
    fecha_entregado: ['', Validators.required],
    observacion_evaluador: ['', Validators.required],
    recomendaciones_futuras: [''],
    nota_evaluador: [null, Validators.required],
    id_documento_entregado: ['', Validators.required],
    url_documento: [''],
    id_proyecto_total: ['', Validators.required],
    evaluadores: this.fb.array([]),
  });
  public historial: any = [];
  public UrlAnteProyectoAprobado: string = '';
  constructor(
    private alerta: AlertasService,
    private loginservice: LoginService,
    private fb: FormBuilder,
    private administrador: AdministradorService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.id_proyecto = parseInt(this.route.snapshot.paramMap.get('id'));
    this.NewForm.patchValue({
      id_project: this.id_proyecto,
    });
    this.CargarDatos();
  }
  // Cargar la informacion detallada del ante proyecto de grado seleccionado para ser evaluado posteriormente
  async CargarDatos() {
    this.historial = [];
    if (this.NewForm.value.evaluadores.length > 0) {
      for (let data of this.NewForm.value.evaluadores) {
        this.EliminarTemporalEvaluadores(data);
      }
    }
    this.administrador
      .DetalleProjectFinalEvaluar({ id_proyecto: this.id_proyecto })
      .subscribe(
        (resp) => {
          var data: any = resp;
          console.log(data);
          let ingreso = 'NO';
          let search = 'C:/xampp/htdocs';
          let replacer = new RegExp(search, 'g');
          if (data.resp.DetalleAnteProyecto.DocumentoProyectoFinal.length > 0) {
            for (let i of data.resp.DetalleAnteProyecto.DocumentoProyectoFinal) {
              let RutaHistorial = i.url_documento.replace(/\\/g, '/');
              let TemporalHistorial = RutaHistorial.replace(
                replacer,
                'http://localhost'
              );
              var push = {
                titulo: data.resp.ProjectEvaluadores.titulo,
                fecha_creacion: i.fecha_creacion,
                conclusion_publica: i.conclusion_publica,
                resumen: i.resumen,
                url_documento: TemporalHistorial,
                observacion_interna_evaluador: i.observacion_interna_evaluador,
                recomendaciones_futuras: i.recomendaciones_futuras,
                notas: i.NotasProyectosFinal,
                estado: i.estado
              };
              this.historial.push(push);
              if (i.estado === null) {
                ingreso = 'SI';
                this.NewForm.patchValue({
                  titulo: data.resp.ProjectEvaluadores.titulo,
                  descripcion: i.descripcion,
                  fecha_entregado: i.fecha_creacion,
                  observacion_evaluador: '',
                  recomendaciones_futuras: '',
                  nota_evaluador: null,
                  id_documento_entregado: i.id,
                  url_documento: TemporalHistorial,
                  id_proyecto_total:
                  data.resp.ProjectEvaluadores.ProyectosFinalesSubidos.id_proyecto,
                });
              }
            }
            let RutaHistorial2 = data.resp.UrlAnteProyecto.replace(/\\/g, '/');
            this.UrlAnteProyectoAprobado = RutaHistorial2.replace(
              replacer,
              'http://localhost'
            );
            // Agregar evaluadores
            if (
              data.resp.ProjectEvaluadores.EvaluadoresAnteProyecto.length > 0
            ) {
              for (let item of data.resp.ProjectEvaluadores
                .EvaluadoresAnteProyecto) {
                this.NewForm.value.evaluadores.push(item);
              }
            }
            if (ingreso === 'NO') {
              this.alerta.error(
                'El proyecto que esta tratando de ver no se encuentra en un estado disponible por evaluar'
              );
              this.router.navigateByUrl('/home');
            }
          } else {
            this.alerta.error(
              'No fue posible relacionar el docuemnto subido pendiente por evaluar'
            );
            this.router.navigateByUrl('/home');
          }
          console.log(this.NewForm.value);
          console.log(this.historial);
        },
        (err: any) => {
          if (!err.error.msg) {
            const errores = err.error.error.map((error: any) => {
              return error.msg;
            });
            this.alerta.error(errores.toString());
          } else {
            this.alerta.error(err.error.msg);
          }
        }
      );
  }
  // Eliminar el evaluador seleccionado
  EliminarTemporalEvaluadores(data) {
    try {
      var i = this.NewForm.value.evaluadores.indexOf(data);
      this.NewForm.value.evaluadores.splice(i, 1);
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
  // Evaluar ante proyecto
  EvaluarProject(){
    try {
      if(this.NewForm.invalid){
        this.alerta.error('Formulario invalidó, por favor agregue todos los campos requeridos');
        console.log(this.NewForm.value);
      }else{
        this.administrador.UpdateNotaProyectoFinal(this.NewForm.value).subscribe(
          (resp) => {
            var data:any = resp;
            this.alerta.exito(data.resp.msg);
            this.router.navigateByUrl('/home/proyectos/tipo-evaluacion');
            console.log(data);
          },(err: any) => {
            if (!err.error.msg) {
              const errores = err.error.error.map((error: any) => {
                return error.msg;
              });
              this.alerta.error(errores.toString());
            } else {
              this.alerta.error(err.error.msg);
            }
          }
        );
      }
    } catch (error) {
      this.alerta.error(error.message);
    }
  }
}
