import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowEvaluarComponent } from './show-evaluar.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgxPaginationModule } from 'ngx-pagination'; // At the top of your module

describe('ShowEvaluarComponent', () => {
  let component: ShowEvaluarComponent;
  let fixture: ComponentFixture<ShowEvaluarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
        NgxPaginationModule,
        HttpClientTestingModule
      ],
      declarations: [ ShowEvaluarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowEvaluarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Inicio de componente', () => {
    expect(component).toBeTruthy();
  });
  it('Validacion de existencia de formulario de calificación', () => {
    const formElement = fixture.debugElement.nativeElement.querySelector('#NewForm');
    const inputElement = formElement.querySelectorAll('input');
    expect(inputElement.length).toEqual(2);
    });
  it('Validar campos de formulario reactivo', () => {
    const fixture = TestBed.createComponent(ShowEvaluarComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
     const login = component.NewForm;
     const loginValores ={
      id_project:  null,
        titulo:  null,
        resumen:  '',
        conclucion: '',
        fecha_entregado: '',
        observacion_evaluador: '',
        recomendaciones_futuras: '',
        estado_aprobacion: '',
        id_documento_entregado: '',
        url_documento: '',
        id_proyecto_seleccionado: '',
       }
     expect(login.value).toEqual(loginValores);
    });
});
