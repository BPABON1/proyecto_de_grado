import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaEvaluarAnteProyectoComponent } from './lista-evaluar-ante-proyecto.component';

describe('ListaEvaluarAnteProyectoComponent', () => {
  let component: ListaEvaluarAnteProyectoComponent;
  let fixture: ComponentFixture<ListaEvaluarAnteProyectoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaEvaluarAnteProyectoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaEvaluarAnteProyectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
