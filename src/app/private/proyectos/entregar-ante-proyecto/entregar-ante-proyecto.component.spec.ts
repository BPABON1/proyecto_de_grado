import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EntregarAnteProyectoComponent } from './entregar-ante-proyecto.component';

describe('EntregarAnteProyectoComponent', () => {
  let component: EntregarAnteProyectoComponent;
  let fixture: ComponentFixture<EntregarAnteProyectoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EntregarAnteProyectoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EntregarAnteProyectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
