import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluarAnteProyectoComponent } from './evaluar-ante-proyecto.component';

describe('EvaluarAnteProyectoComponent', () => {
  let component: EvaluarAnteProyectoComponent;
  let fixture: ComponentFixture<EvaluarAnteProyectoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EvaluarAnteProyectoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluarAnteProyectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
