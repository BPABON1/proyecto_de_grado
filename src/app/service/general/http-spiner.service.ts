import { Injectable } from '@angular/core';
import { SpinnerVisibilityService } from 'ng-http-loader';

@Injectable({
  providedIn: 'root'
})
export class HttpSpinerService {

  constructor(
    private spinner: SpinnerVisibilityService
  ) { }
  ShowSpinner(){
    this.spinner.show();
  }
  HiddeSpinner(){
    this.spinner.hide();
  }
  EnviarTocken(){
    return localStorage.getItem('token') || '';
  }
}
