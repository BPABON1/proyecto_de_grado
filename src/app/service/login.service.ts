import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

const API = environment.API;
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }
  /*
  * autor: branm aldair pabon villamizar
  * proposito: verificar el usuario y la contraseña en el sistema para cargar el inicio
  */
  login(datos:any){
    return this.http.put(`${API}/project/login`, datos)
    .pipe(
        map(resp => { return {resp }  })
    )
  }
  /*
  * proposito: Cargar la lista de usuarios con los respectivos roles
  */
    UsuariosRoles(){
      return this.http.get(`${API}/project/list_user_rol`)
      .pipe(
          map(resp => { return {resp }  })
      )
    }
      /*
   * proposito: Mostrar Todos los proyectos de grado pendientes por evaluar
   */
      LitProjectsEvaluar() {
        return this.http
          .get(`${API}/project/ListProjectEvaluar`, {
            headers: {
              'x-token': localStorage.getItem('token') || '',
            },
          })
          .pipe(
            map((resp) => {
              return { resp };
            })
          );
      }
  /*
  * autor: branm aldair pabon villamizar
  * proposito: Restablecer contraseña de usuario
  */
  UpdatePassword(datos:any){
    return this.http.put(`${API}/project/UpdatePassword`, datos)
    .pipe(
        map(resp => { return {resp }  })
    )
  }
    /*
  * autor: branm aldair pabon villamizar
  * proposito: Crear la nueva contraseña
  */
    NewPassword(password:string,id: string){
      return this.http.put(`${API}/project/NewPassword`, {password: password, id: id})
      .pipe(
          map(resp => { return {resp }  })
      )
    }
/*
* proposito: Mostrar Todos los proyectos de grado pendientes por evaluar
*/
LitAnteProjectsEvaluar() {
  return this.http
    .get(`${API}/project/ListAnteProjectEvaluar`, {
      headers: {
        'x-token': localStorage.getItem('token') || '',
      },
    })
    .pipe(
      map((resp) => {
        return { resp };
      })
    );
}
/*
* proposito: Mostrar Todos los proyectos de grado pendientes por evaluar
*/
LitProjectsFinalEvaluar() {
  return this.http
    .get(`${API}/project/ListProjectFinalEvaluar`, {
      headers: {
        'x-token': localStorage.getItem('token') || '',
      },
    })
    .pipe(
      map((resp) => {
        return { resp };
      })
    );
}
}
