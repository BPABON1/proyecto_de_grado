import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpSpinerService } from 'src/app/service/general/http-spiner.service';
const API = environment.API;
@Injectable({
  providedIn: 'root',
})
export class AdministradorService {
  constructor(
    private http: HttpClient,
    private EnviarTocken: HttpSpinerService
  ) {}
  /*
   * proposito: Registrar el nuevo usuario
   */
  RegistrerUser(datos: any) {
    return this.http
      .post(`${API}/project/registerUser`, datos, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
  // Actualizar la informacion del usuario seleccionado
  UpdateUser(datos: any) {
    return this.http
      .put(`${API}/project/UpdateUser`, datos, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
  /*
   * proposito: Cargar la lista de estudiantes y de projectos creados
   */
  ProjectsEstudiantes() {
    return this.http.get(`${API}/project/list_student_projects`).pipe(
      map((resp) => {
        return { resp };
      })
    );
  }
  /*
   * proposito: Registrar el proyecto y los estudiantes a cargo de realizar
   */
  RegisterProjects(datos: any) {
    return this.http
      .post(`${API}/project/registerProjects`, datos, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
  /*
   * proposito: Registrar el proyecto y los estudiantes a cargo de realizar
   */
  UpdateProjects(datos: any) {
    return this.http
      .post(`${API}/project/UpdateProjects`, datos, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
  /*
   * proposito: Eliminar la asociacion del estudiante con el proyecto seleccionado
   */
  DeleteProjects(datos: any) {
    return this.http
      .put(`${API}/project/DeleteProjects`, datos, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
  /*
   * proposito: Mostrar informacion de los ptoyectos subidos y los pendientes
   */
  ShowSelectProjects() {
    return this.http
      .get(`${API}/project/DetalleProjectPersonal`, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
  //
  /*
   * fecha: 28-09-2021
   * autor: branm aldair pabon villamizar
   * proposito: Realizar la subida de la imagen a la nube y asociar en tabla de hc_documentos
   */
  async SubirDocumento(
    archivo: File,
    idproject: string,
    resumen: string,
    concluciones: string
  ) {
    try {
      const url = `${API}/project/UploadProject/${idproject}/${resumen}/${concluciones}`;
      const formData = new FormData();
      formData.append('archivo', archivo);

      const resp = await fetch(url, {
        method: 'PUT',
        body: formData,
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      });

      const data = await resp.json();
      if (data) {
        return data;
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }
  /*
   * proposito: Cargar la informacion detallada del proyecto a evaluar
   */
  DetalleProjectEvaluar(datos: any) {
    return this.http
      .put(`${API}/project/DetalleProjectsEvaluar`, datos, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
  /*
   * proposito: Cargar los datos de recomendaciones y observaciones y estado evaluado
   */
  EvaluarProjectSelected(datos: any) {
    return this.http
      .put(`${API}/project/EvaluarEstadoProjects`, datos, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
  /*
   * proposito: Cargar los datos de recomendaciones y observaciones y estado evaluado
   */
  ListarProjectPublicos() {
    return this.http
      .get(`${API}/project/ProjectsPublicos`, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
  /*
   * proposito: Cambiar el estado publico o privado de la tesis de grado aprovada previamente
   */
  UpdateStatusProjects(datos: any) {
    return this.http
      .put(`${API}/project/UpdatePublicoProjects`, datos, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
  /*
   * proposito: Cargar los datos de recomendaciones y observaciones y estado evaluado
   */
  ListarProjectPublicados() {
    return this.http
      .get(`${API}/project/ProjectsBuscadorAprobados`, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
  /*
   * proposito: Cargar el detalle del proyecto de grado publicado y seleccionado
   */
  DetalleProyectoPublicado(id: number) {
    return this.http
      .get(`${API}/project/DetalleProyectoPublicado/${id}`, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
  /*
   * proposito: Listar el total de proyectos y sus respectivas fases de ante proyecto o proyecto de grado
   */
  SearchFasesProyectos() {
    return this.http.get(`${API}/project/AsignacionAnteProyectos`).pipe(
      map((resp) => {
        return { resp };
      })
    );
  }
  /*
   * proposito: Registar ante proyecto en la base de datos
   */
  SaveAnteProyectos(datos) {
    return this.http
      .post(`${API}/project/SaveAnteProyecto`, datos, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
  /*
   * proposito: Registar ante proyecto en la base de datos
   */
  DeleteEstudianteAnteProyectos(datos) {
    return this.http
      .put(`${API}/project/DeleteEstudianteAnteProyectos`, datos, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
  /*
   * proposito: Registar ante proyecto en la base de datos
   */
  DeleteDirectorAnteProyectos(datos) {
    return this.http
      .put(`${API}/project/DeleteDirectorAnteProyectos`, datos, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
  /*
   * proposito: Registar ante proyecto en la base de datos
   */
  DeleteEvaluadorAnteProyectos(datos) {
    return this.http
      .put(`${API}/project/DeleteEvaluadorAnteProyectos`, datos, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
  /*
   * proposito: Registar ante proyecto en la base de datos
   */
  UpdateAnteProyectos(datos) {
    return this.http
      .post(`${API}/project/UpdateAnteProyecto`, datos, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
  /*
   * proposito: Mostrar informacion de los ptoyectos subidos y los pendientes
   */
  ShowSelectAnteProjects() {
    return this.http
      .get(`${API}/project/DetalleAnteProjectPersonal`, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
  /*
   * fecha: 28-09-2021
   * autor: branm aldair pabon villamizar
   * proposito: Realizar la subida de la imagen a la nube y asociar en tabla de hc_documentos
   */
  async SubirDocumentoAnteProyecto(
    archivo: File,
    idproject: string,
    observaciones: string
  ) {
    try {
      const url = `${API}/project/UploadAnteProject/${idproject}/${observaciones}`;
      const formData = new FormData();
      formData.append('archivo', archivo);

      const resp = await fetch(url, {
        method: 'PUT',
        body: formData,
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      });

      const data = await resp.json();
      if (data) {
        return data;
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }
  /*
   * proposito: Cargar la informacion detallada del proyecto a evaluar
   */
  DetalleAnteProjectEvaluar(datos: any) {
    return this.http
      .put(`${API}/project/DetalleAnteProjectsEvaluar`, datos, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
  /*
   * proposito: Evaluar el ante proyecto y asignar la nueva nota y si es el caso determinar el estado final del proyecto
   */
  UpdateNotaAnteProyecto(datos: any) {
    return this.http
      .put(`${API}/project/EvaluarAnteProjectsEvaluar`, datos, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
    /*
   * proposito: Registar proyecto final en la base de datos
   */
    DeleteDirectorProyectosFinal(datos) {
      return this.http
        .put(`${API}/project/DeleteDirectorProyectosFinal`, datos, {
          headers: {
            'x-token': localStorage.getItem('token') || '',
          },
        })
        .pipe(
          map((resp) => {
            return { resp };
          })
        );
    }
      /*
   * proposito: Registar ante proyecto en la base de datos
   */
  DeleteEvaluadorProyectosFinal(datos) {
    return this.http
      .put(`${API}/project/DeleteEvaluadorProyectosFinal`, datos, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
    /*
   * proposito: Atualizar proyecto final en la base de datos
   */
    UpdateProyectosFinal(datos) {
      return this.http
        .post(`${API}/project/UpdateProyectoFinal`, datos, {
          headers: {
            'x-token': localStorage.getItem('token') || '',
          },
        })
        .pipe(
          map((resp) => {
            return { resp };
          })
        );
    }
  /*
   * proposito: Mostrar informacion de los ptoyectos subidos y los pendientes
   */
  ShowSelectProjectsFinal() {
    return this.http
      .get(`${API}/project/DetalleProjectFinalPersonal`, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
    /*
   * fecha: 28-09-2021
   * autor: branm aldair pabon villamizar
   * proposito: Realizar la subida de la imagen a la nube y asociar en tabla de hc_documentos
   */
    async SubirDocumentoProyectoFinal(
      archivo: File,
      idproject: string,
      observaciones: string
    ) {
      try {
        const url = `${API}/project/UploadProjectFinal/${idproject}/${observaciones}`;
        const formData = new FormData();
        formData.append('archivo', archivo);

        const resp = await fetch(url, {
          method: 'PUT',
          body: formData,
          headers: {
            'x-token': localStorage.getItem('token') || '',
          },
        });

        const data = await resp.json();
        if (data) {
          return data;
        } else {
          return false;
        }
      } catch (error) {
        return false;
      }
    }
  /*
  * proposito: Cargar la informacion detallada del proyecto a evaluar
  */
  DetalleProjectFinalEvaluar(datos: any) {
    return this.http
      .put(`${API}/project/DetalleProjectsFinalEvaluar`, datos, {
        headers: {
          'x-token': localStorage.getItem('token') || '',
        },
      })
      .pipe(
        map((resp) => {
          return { resp };
        })
      );
  }
    /*
   * proposito: Evaluar el proyecto final y asignar la nueva nota y si es el caso determinar el estado final del proyecto
   */
    UpdateNotaProyectoFinal(datos: any) {
      return this.http
        .put(`${API}/project/EvaluarProjectsFinalEvaluar`, datos, {
          headers: {
            'x-token': localStorage.getItem('token') || '',
          },
        })
        .pipe(
          map((resp) => {
            return { resp };
          })
        );
    }
}
