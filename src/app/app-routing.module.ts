import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './public/login/login.component';
import { NotFountComponent } from './public/not-fount/not-fount.component';
import { HomeComponent } from './private/home/home.component';
import { PrivateModule } from './private/private.module';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  { path: 'error', component: NotFountComponent },
  { path: 'login', component: LoginComponent,pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () => import('./private/private.module').then(m => m.PrivateModule)

  },
  // { path: 'home', component: HomeComponent },
  {
    path: '**', redirectTo: 'login',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
