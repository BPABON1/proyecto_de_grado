import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule} from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {AppComponent} from 'src/app/app.component';
describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule,
        ReactiveFormsModule,
        HttpClientTestingModule
      ],
      declarations: [ LoginComponent ],
      providers:[AppComponent]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Inicio de componente', () => {
    const fixture = TestBed.createComponent(LoginComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
  it('Cantidad de elementos en el formulario reactivo', () => {
    const formElement = fixture.debugElement.nativeElement.querySelector('#FormularioLogin');
    const inputElement = formElement.querySelectorAll('input');
    expect(inputElement.length).toEqual(2);
    });
  it('Probar valores Iniciales en el formulario reactivo', () => {
    const login = component.FormularioLogin;
    const loginValores ={
      user:null,
      password:'',
    }
    expect(login.value).toEqual(loginValores);
    });
});
