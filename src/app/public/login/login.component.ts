import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertasService } from 'src/app/service/alertas.service';
import {LoginService} from 'src/app/service/login.service';
import {HttpSpinerService} from 'src/app/service/general/http-spiner.service';
import "animate.css";
import {AppComponent} from 'src/app/app.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: []
})
export class LoginComponent implements OnInit {
  public FormularioLogin = this.fb.group({
    user: [null, Validators.required],
    password: ['', Validators.required],
  });
  public UpdatePassword = this.fb.group({
    user: [null, Validators.required],
  });
  public hide = true;
  constructor(
    private route: Router,
    private spinner: HttpSpinerService,
    private fb: FormBuilder,
    private alerta: AlertasService,
    private loginservice: LoginService,
    private app: AppComponent,
    private modalService: NgbModal,
  ) { }

  ngOnInit(): void {
    this.app.ValidarSesionIniciada();
  }
  /*
  * PROPOSITO: valida que se aya ingresado un usuario y una contraseña para loguearse en el sistema
  */
  InisiarSesion(){
    try {
      if (this.FormularioLogin.invalid) {
        this.alerta.info('Se deben ingresar un usuario y contraseña valida');
      }
      else {
        this.spinner.ShowSpinner();
        // ENVIAR PETICION AL SERVIDOR PARA VALIDAR CON BASE DE DATOS
        this.loginservice.login(this.FormularioLogin.value).subscribe(resp => {
          // RESPUESTA EXITOSA
          var data:any = resp;
          this.spinner.HiddeSpinner();
          this.alerta.exito(data.resp.msg);
          let permisos = data.resp.permisos.map((info:any) => { return info.Permiso.codigo });
          // ALMACENAR LA INFORMACION EN LOCAL STORAGE
          localStorage.setItem('permisos', JSON.stringify(permisos));
          localStorage.setItem('usuario', JSON.stringify(data.resp.user));
          localStorage.setItem('token', data.resp.token);
          this.route.navigateByUrl('/home');
        }, (err) => {
          // MOSTRAR ALERTAS EN CASO DE RECIBIR ERROR 404-__****_
          console.log(err);
          this.spinner.HiddeSpinner();
          if(err.error.data){
            this.NewPassword(err.error.data);
          }else{
            if(err.error.msg){
              this.alerta.error(err.error.msg);
            }else{
              this.alerta.error(err.message);
            }
          }
        });
      }
    } catch (err:any) {
      this.alerta.error(err.message);
    }
  }
  /*
  * PROPOSITO: Peticion para restablecer contraseña
  */
 async PutUpdatePassword(){
  try {
    if(this.UpdatePassword.invalid){
      this.alerta.info('Usuario requerido');
    }else{
      this.spinner.ShowSpinner();
        // ENVIAR PETICION AL SERVIDOR PARA VALIDAR CON BASE DE DATOS
        this.loginservice.UpdatePassword(this.UpdatePassword.value).subscribe(resp => {
          var data:any = resp;
          this.spinner.HiddeSpinner();
          this.alerta.exito(data.resp.msg);
          console.log(data);
        }, (err) => {
          // MOSTRAR ALERTAS EN CASO DE RECIBIR ERROR 404-__****_
          this.spinner.HiddeSpinner();
          if (!err.error.msg) {
            const errores = err.error.error.map((error:any) => { return error.msg });
            this.alerta.error(errores.toString());
          }else{
            this.alerta.error(err.error.msg);
          }
        });
    }
  } catch (error) {
    this.alerta.error(error.message);
  }
 }
 /*
 proposito: Agregar una nueva contraseña
 */
async NewPassword(datos:any){
  try {
    const { value: password } = await Swal.fire({
      title: 'Nueva contraseña',
      input: 'text',
      inputPlaceholder: 'Ingrese la nueva contraseña',
      showCancelButton: true,
      inputValidator: (value) => {
        if (!value) {
          return 'La contraseña es requerida'
        }
      }
    });
    if (password) {
      this.spinner.ShowSpinner();
      // ENVIAR PETICION AL SERVIDOR PARA VALIDAR CON BASE DE DATOS
      this.loginservice.NewPassword(password,datos.id).subscribe(resp => {
        var data:any = resp;
        this.spinner.HiddeSpinner();
        this.alerta.exito(data.resp.msg);
      }, (err) => {
        // MOSTRAR ALERTAS EN CASO DE RECIBIR ERROR 404-__****_
        this.spinner.HiddeSpinner();
        if (!err.error.msg) {
          const errores = err.error.error.map((error:any) => { return error.msg });
          this.alerta.error(errores.toString());
        }else{
          this.alerta.error(err.error.msg);
        }
      });
    }
  } catch (error) {
    this.alerta.error(error.message);
  }
}
}
